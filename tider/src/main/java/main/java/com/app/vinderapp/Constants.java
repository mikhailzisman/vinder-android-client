package main.java.com.app.vinderapp;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 14.05.14
 * Time: 12:42
 */
public class Constants {

    public final static String IS_HINT_SHOULD_BE_SHOWN = "hint_should_be_shown";
    public static final String SCREEN_WIDTH = "screen_width";

    public static final String SCREEN_HEIGHT = "screen_height";


    //Shared preferences
    public static final String USER_VK_INFO_SHARED_PREFERENCE = "user";
    public static final String USER_ID_SHARED_PREFERENCE = "user_id";

    //production == true
    public static final boolean IAB_ENABLED = false;
}
