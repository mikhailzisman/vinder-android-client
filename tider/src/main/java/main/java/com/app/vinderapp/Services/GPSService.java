package main.java.com.app.vinderapp.Services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import main.java.com.app.vinderapp.Config;
import main.java.com.app.vinderapp.IOC.Injector;

import main.java.com.app.vinderapp.Utils.UserProvider;
import com.squareup.otto.Bus;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.06.2014
 * Time: 14:26
 */
public class GPSService extends Service implements LocationListener {

    private static final String TAG = "GPS";
    private Location mPreviousLocation = null;

    @Inject
    RestManager restManager;

    @Inject
    UserProvider userProvider;

    public static final String GPS_PROVIDER = "GPS_PROVIDER";
    private static Location my_location;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Inject
    protected Bus BUS;


    @Override
    public void onCreate() {
        super.onCreate();

        Injector.inject(this);

        register();

        turnGPSOn();

        LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //if (locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
        try {
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        } catch (IllegalArgumentException ex){
            Log.e(TAG,ex.getMessage().toString());
        }


        locManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, Config.GPS_UPDATE_TIME_MILLIS, 1, this);//15
        //}

    }

    @Override
    public void onDestroy() {

        unregister();

        super.onDestroy();
    }
    ///////////////////////////////////////////////////////////


    @Override
    public void onLocationChanged(final Location location) {
        this.my_location = location;


        if ((restManager!=null)&&(userProvider.load()!=null))
        restManager.vinderService.updateLocation(userProvider.load().getID(),location);
    }

    public static Location getMy_location() {
        return my_location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        BUS.post(new ProviderStatusChangedEvent(provider, status, extras));
    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }


    private void produceProviderChangedState(String provider, boolean b) {
        //it works by user - not important
    }



    ///////////////////////GPS////////////////////////////////////////
    public void turnGPSOn() {
        Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
        intent.putExtra("enabled", true);
        this.sendBroadcast(intent);

        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            this.sendBroadcast(poke);


        }
    }

    // automatic turn off the gps
    public void turnGPSOff() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        if (provider.contains("gps")) { //if gps is enabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            this.sendBroadcast(poke);
        }
    }

    public void register() {
        BUS.register(this);
    }

    public void unregister() {
        BUS.unregister(this);
    }

}
