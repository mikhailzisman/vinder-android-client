package main.java.com.app.vinderapp.UI;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import main.java.com.app.vinderapp.Constants;
import main.java.com.app.vinderapp.IOC.Injector;
import main.java.com.app.vinderapp.Models.Friendship;
import main.java.com.app.vinderapp.Models.Like;
import main.java.com.app.vinderapp.Models.Photo;
import main.java.com.app.vinderapp.Models.User;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.GPSService;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.*;
import main.java.com.app.vinderapp.Widget.RobotoThinTextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import org.json.JSONException;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;
import java.util.ArrayList;

import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static android.widget.RelativeLayout.LayoutParams;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.05.14
 * Time: 9:49
 */
public class VoteFragment extends Fragment {

    private static final int HISTORY = 1;
    private static final int STARTED_VOTE_FRAGMENT = 200;
    @InjectView(R.id.wasTime)
    RobotoThinTextView mWasTime;
    @InjectView(R.id.pro)
    RobotoThinTextView mPro;
    private ImageSwitcher imageSwitcher;

    @InjectView(R.id.name)
    TextView name;

    @InjectView(R.id.friends)
    TextView friends;

    @InjectView(R.id.distance)
    TextView dsitance;

    @InjectView(R.id.notUserFoundLayout)
    LinearLayout notUserFoundLayout;

    @InjectView(R.id.userInfoLayout)
    LinearLayout userInfoLayout;
    private ProgressDialog progressDialog;

    @Inject
    UserProvider userProvider;
    private Photo curPhoto;

    private Like likeFromHistory = null;


    @OnClick(R.id.changeSettings)
    public void onChangeSettings() {
        startActivity(new Intent(getActivity(), SettingsActivity.class));
    }

    @Inject
    RestManager restManager;

    ViewSwitcher.ViewFactory factory = new ViewSwitcher.ViewFactory() {

        @Override
        public View makeView() {
            ImageView photoImageView = new ImageView(getActivity());
            photoImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            photoImageView.setLayoutParams(new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.
                    MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            return photoImageView;
        }

    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Injector.inject(this);


        View contentView = inflater.inflate(R.layout.vote_layout, null);


        ButterKnife.inject(this, contentView);

        assert contentView != null;
        LinearLayout description_panel = (LinearLayout) contentView.findViewById(R.id.description_panel);
        float weight = AccessPreferences.get(getActivity(), Constants.SCREEN_HEIGHT, 1280) > 900 ? 1.0f : 1.6f;
        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                0, weight);
        description_panel.setLayoutParams(param);


        imageSwitcher = (ImageSwitcher) contentView.findViewById(R.id.imageSwitcher1);

        ImageButton like = (ImageButton) contentView.findViewById(R.id.btnLike);
        ImageButton dislike = (ImageButton) contentView.findViewById(R.id.btnDislike);

        ImageButton btnHistory = (ImageButton) contentView.findViewById(R.id.btnHistory);
        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoHistrory();
            }
        });

        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (curPhoto != null)
                    like();
            }
        });

        dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (curPhoto != null)
                    dislike();
            }
        });

        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                gotoBuddyProfily();
            }
        });

        imageSwitcher.setFactory(factory);

        imageSwitcher.setOnTouchListener(mActivitySwipeMotion);

        updateUndeliveredInvitations();

        return contentView;
    }


    private void startFirendshipInvitationActivity(Friendship friendship) {
        if (friendship == null) return;
        Intent dialogIntent = new Intent(getActivity(), InvitationFragmentActivity.class);
        dialogIntent.putExtra(InvitationFragmentActivity.INVITATION, friendship);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivityForResult(dialogIntent, STARTED_VOTE_FRAGMENT);
    }


    @Override
    public void onResume() {
        super.onResume();

        if (!AccessPreferences.get(getActivity(), SettingsActivity.USER_SETTINGS_IS_DEFINED, false)) {
            startActivity(new Intent(getActivity(), SettingsActivity.class));
            return;
        }
//        if (likeFromHistory != null) {
//            if (checkGPS()) {
////                startLoadUsers();
//            }
//        }


        if (likeFromHistory != null) {
            showPhoto(likeFromHistory.getPhoto());
        } else
            pollPhotoAndShow();

    }

    private boolean checkGPS() {
        if (isGPSEnabled()) {

            if (GPSService.getMy_location() != null) return true;

            Toast.makeText(getActivity(), "Мы не можем определить ваше положение!", Toast.LENGTH_LONG).show();
//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Определяем ваше положение...");
//            progressDialog.show();
            return false;
        } else {

            AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
            adb.setMessage("Ваше положение не определено, включите GPS").setPositiveButton("Включить", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            }).setNegativeButton("Нет, спасибо", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            }).show();
        }

        return false;
    }

    private void updateUndeliveredInvitations() {

        restManager.vinderService.getFriendshipsToMe(userProvider.load().getID(), new Callback<ArrayList<Friendship>>() {
            @Override
            public void success(ArrayList<Friendship> friendships, Response response) {
                for (Friendship friendship : friendships) {
                    if (!friendship.getUser1Delivered() && friendship.getUser1() == friendship.getMe() ||
                            !friendship.getUser2Delivered() && friendship.getUser2() == friendship.getMe()) {
                        startFirendshipInvitationActivity(friendship);
                        return;
                    }
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    public void setVisiblity(int visiblity) {

        imageSwitcher.setVisibility(visiblity);


        userInfoLayout.setVisibility(visiblity);
        name.setVisibility(visiblity);
        mWasTime.setVisibility(visiblity);

        mPro.setVisibility(visiblity);
        dsitance.setVisibility(visiblity);
        friends.setVisibility(visiblity);
    }

    private void showWasDistanceAndTime(User curUser) {

        StringBuilder sb = new StringBuilder();

        String date = DateUtils.fromDatabaseDate(curUser.getUpdatedDate()).replace(",", " в ");

        sb.append(curUser.getSex() != null && curUser.getSex() == 1 ? "Была " : "Был ");
        sb.append(date);

        mWasTime.setText(sb.toString());

        if ((curUser.getLatitude() == null) || (curUser.getLongitude() == null))
            return;

        Location gpsLocation = new Location("GPS");
        gpsLocation.setLatitude(curUser.getLatitude());
        gpsLocation.setLongitude(curUser.getLongitude());


        if (GPSService.getMy_location() == null) return;

        float distance = DistanceUtils.distFrom(gpsLocation.getLatitude(), gpsLocation.getLongitude(),
                GPSService.getMy_location().getLatitude(), GPSService.getMy_location().getLongitude());

        float km = distance / 1000;
        sb.append(" всего в " + String.format("%.1f", km) + " км от вас");
        mWasTime.setText(sb.toString());

        dsitance.setVisibility(View.INVISIBLE);
        dsitance.setText(String.format("%.1f", km) + "км");

    }

    private void pollPhotoAndShow() {
        restManager.vinderService.getNextPhoto(userProvider.load().getID(), new Callback<Photo>() {
            @Override
            public void success(Photo photo, Response response) {
                showPhoto(photo);
                setVisiblity(VISIBLE);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                setVisiblity(INVISIBLE);
                notUserFoundLayout.setVisibility(VISIBLE);
            }
        });

    }

    private void showPhoto(Photo photo) {
        setVisiblity(VISIBLE);
        notUserFoundLayout.setVisibility(INVISIBLE);
        curPhoto = photo;
        ImageLoader.getInstance().displayImage(photo.getMaxResolutionUrl(), (ImageView) imageSwitcher.getCurrentView());

        if (photo.user != null) {
            showNameAndFriendsCount(photo.user);
            showWasDistanceAndTime(photo.user);
        }
    }


    private void gotoHistrory() {
        startActivityForResult(new Intent(getActivity(), VoteHistoryActivity.class), HISTORY);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == VoteHistoryActivity.HISTORY_LIKE_RESULT_CODE) {

            likeFromHistory = (Like) data.getSerializableExtra(VoteHistoryActivity.HISTORY_LIKE);

        }

        //if (resultCode == InvitationFragmentActivity.CONTINUE_REQUEST_CODE)
    }


    private void showNameAndFriendsCount(User user) {
        name.setText(user.getFirst_name() + " "
                + (user.vip_status && user.getSettings().getHide_age() ? "" : UserInfoUtils.getFullAgesString(user.getAge())));

        startGetMutualFriendsCount(user.getUid());
    }

    private void startGetMutualFriendsCount(final Long uid) {
        VKParameters vkParameters = new VKParameters();
        vkParameters.put("target_uid", uid);
        vkParameters.put("source_uid", VKSdk.getAccessToken().userId);
        VKRequest request = VKApi.friends().getMutual(vkParameters);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                int mutual_friends_count = 0;

                if (response.json.has("response"))
                    try {
                        mutual_friends_count = response.json.getJSONArray("response").length();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                friends.setVisibility(View.INVISIBLE);
                friends.setText(String.valueOf(mutual_friends_count));
            }
        });

    }

    public void like() {

        Animation out_anim = AnimationUtils.loadAnimation(getActivity(),
                R.anim.like_out_photo_anim);

        Animation in_anim = AnimationUtils.loadAnimation(getActivity(),
                R.anim.in_photo_anim);

        imageSwitcher.setInAnimation(in_anim);
        imageSwitcher.setOutAnimation(out_anim);

        sendLike(curPhoto, true);


    }

    public void dislike() {
        Animation in_anim = AnimationUtils.loadAnimation(getActivity(),
                R.anim.in_photo_anim);

        Animation out_anim = AnimationUtils.loadAnimation(getActivity(),
                R.anim.dislike_out_photo_anim);

        imageSwitcher.setInAnimation(in_anim);
        imageSwitcher.setOutAnimation(out_anim);

        sendLike(curPhoto, false);


    }

    private void sendLike(Photo photo, final Boolean like_result) {

        if (likeFromHistory != null) {
            likeFromHistory.setResult(like_result);
            restManager.vinderService.updateLike(likeFromHistory, new Callback<Like>() {
                @Override
                public void success(Like like, Response response) {
                    if (like != null) {
                        Toast.makeText(getActivity(), "Результат обновлен", Toast.LENGTH_LONG).show();

                        if (like.getResult())
                            updateUndeliveredInvitations();

                    }

                    pollPhotoAndShow();

                    likeFromHistory = null;
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    Toast.makeText(getActivity(), "Ошибка обновления результата", Toast.LENGTH_LONG).show();

                    likeFromHistory = null;
                }
            });


            return;
        }

        Like like = new Like();
        like.setPhoto(photo);
        like.setResult(like_result);
        like.setWho(userProvider.load());
        like.setWhom(photo.user);

        restManager.vinderService.createLike(like, new Callback<Like>() {
            @Override
            public void success(Like like, Response response) {
                Toast.makeText(getActivity(), "Вы проголосовали", Toast.LENGTH_LONG).show();

                pollPhotoAndShow();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(getActivity(), "Ошибка при голосовании", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void gotoBuddyProfily() {
        Intent intent = new Intent(getActivity(), ProfileBuddy.class);
        intent.putExtra("user", curPhoto.user);
        startActivity(intent);
    }

    private String TAG = getClass().getSimpleName();
    ActivitySwipeMotion mActivitySwipeMotion = new ActivitySwipeMotion(getActivity()) {
        public void onSwipeLeft() {
            Boolean is_hint_should_be_shown = AccessPreferences.get(getActivity(), Constants.IS_HINT_SHOULD_BE_SHOWN, true);
            if (is_hint_should_be_shown) {
                startLikeHintDialog(false);
                return;
            }
            dislike();
            Log.i(TAG, "Swiping Left");
        }

        public void onSwipeRight() {
            Boolean is_hint_should_be_shown = AccessPreferences.get(getActivity(), Constants.IS_HINT_SHOULD_BE_SHOWN, true);
            if (is_hint_should_be_shown) {
                startLikeHintDialog(true);
                return;
            }
            like();
            Log.i(TAG, "Swiping Right");
        }

        public void onSwipeDown() {
            Log.i(TAG, "Swiping Down");
        }

        public void onSwipeUp() {
            like();
            Log.i(TAG, "Swiping Up");
        }

        @Override
        public void onClick() {
            if (curPhoto != null && curPhoto.user != null)
                gotoBuddyProfily();
        }
    };

    private void startLikeHintDialog(boolean like) {

        AccessPreferences.commit(getActivity(), Constants.IS_HINT_SHOULD_BE_SHOWN, false);

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        int message_res = like ? R.string.hint_like : R.string.hint_not_like;
        adb.setTitle("Подсказка")
                .setMessage(message_res)
                .setCancelable(true)
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dislike();
                    }
                }).setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                like();
            }
        }).show();
    }


    public boolean isGPSEnabled() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
            return true;
        else
            return false;
    }

}
