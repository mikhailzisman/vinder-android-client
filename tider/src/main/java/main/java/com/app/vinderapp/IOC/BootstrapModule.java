package main.java.com.app.vinderapp.IOC;

import main.java.com.app.vinderapp.Models.User;
import main.java.com.app.vinderapp.Services.*;
import main.java.com.app.vinderapp.UI.*;
import main.java.com.app.vinderapp.Utils.UserProvider;
import main.java.com.app.vinderapp.chat.ChatActivity;
import main.java.com.app.vinderapp.chat.DiscussArrayAdapter;
import com.google.gson.*;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;
import java.util.Date;

/**
 * Dagger module for setting up provides statements.
 * Register all of your entry points below.
 */
@Module
        (
                complete = false,
                library = true,
                injects = {
                        AddPhotoActivity.class,
                        ProfileActivity.class,
                        RestManager.class,
                        BootstrapApplication.class,
                        ProfileBuddy.class,
                        VoteHistoryActivity.class,
                        ChatActivity.class,
                        HomeActivity.class,
                        MessagesListActivity.class,
                        UserProvider.class,
                        SettingsActivity.class,
                        LoginActivity.class,
                        VoteFragment.class,
                        UserProvider.class,
                        ImageAdater.class,
                        GPSService.class,
                        SocketService.class,
                        CommonService.class,
                        InvitationFragmentActivity.class,
                        MessagesListAdapter.class,
                        DiscussArrayAdapter.class,
                        LikeListAdapter.class
                }

        )
public class BootstrapModule {

    @Singleton
    @Provides
    Bus provideBUS() {
        return new Bus(ThreadEnforcer.ANY);
    }

    @Singleton
    @Provides
    RestManager provideRestManager() {
        return new RestManager();
    }

    @Singleton
    @Provides
    Gson provideGson() {
//        final Gson gson = Converters
//                .registerDateTime(new GsonBuilder())
//                //.registerTypeAdapter(Date.class, new DateDeserializer())
//                .create();
        //return Converters.registerDateTime(new GsonBuilder().registerTypeAdapter(DateTime.class, new DateTimeTypeAdapter())).create();

        return new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateConverter())
                .create();
    }

    @Singleton
    @Provides
    UserProvider provideUserProvider() {
        return new UserProvider();
    }


}
