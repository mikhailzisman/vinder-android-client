package main.java.com.app.vinderapp.Models;


import com.vk.sdk.VKSdk;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 07.08.2014
 * Time: 20:01
 */
public class Friendship implements Serializable {

    Long ID;

    User user1;

    User user2;

    Like likeToUser1;

    Like likeToUser2;

    Boolean user1Delivered;

    Boolean user2Delivered;

    public Boolean getUser2Delivered() {
        return user2Delivered;
    }

    public void setUser2Delivered(Boolean user2Delivered) {
        this.user2Delivered = user2Delivered;
    }

    public void setLikeToUser2(Like likeToUser2) {
        this.likeToUser2 = likeToUser2;
    }

    public void setLikeToUser1(Like likeToUser1) {
        this.likeToUser1 = likeToUser1;
    }

    public Boolean getUser1Delivered() {
        return user1Delivered;
    }

    public void setUser1Delivered(Boolean user1Delivered) {
        this.user1Delivered = user1Delivered;
    }

    public Long getID() {
        return ID;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public Like getLikeToUser1() {
        return likeToUser1;
    }

    public Like getLikeToUser2() {
        return likeToUser2;
    }

    public User getMe(){
        if (getUser1().getUid().toString().equals(VKSdk.getAccessToken().userId))
           return getUser1();

        if (getUser2().getUid().toString().equals(VKSdk.getAccessToken().userId))
            return getUser2();

        throw new IllegalArgumentException("В дружбе нет человека с моим userId");
    }

    public User getHim(){
        return getMe() == getUser1()?getUser2():getUser1();
    }
}
