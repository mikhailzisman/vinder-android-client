package main.java.com.app.vinderapp.UI;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import butterknife.InjectView;
import main.java.com.app.vinderapp.IOC.Injector;
import main.java.com.app.vinderapp.Models.Like;
import main.java.com.app.vinderapp.Models.User;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.DateUtils;
import main.java.com.app.vinderapp.Utils.UserInfoUtils;
import main.java.com.app.vinderapp.Widget.RobotoThinTextView;
import main.java.com.app.vinderapp.Widget.TypefacedTextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.05.2014
 * Time: 11:05
 */
public class LikeListAdapter extends ArrayAdapter<Like> {

    @Inject
    RestManager restManager;

    private final LayoutInflater inflater;
    private Context context;
    private List<Like> likes;

    //2014-06-09T05:19:30.095Z
    SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    SimpleDateFormat sdfOut = new SimpleDateFormat("HH:mm");

    @InjectView(R.id.imageView)
    ImageView imageView;
    private ViewHolder viewHolder;


    public LikeListAdapter(Context context, int resource, List<Like> objects) {
        super(context, resource, objects);
        this.context = context;
        this.likes = objects;
        inflater = LayoutInflater.from(context);

        Injector.inject(this);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.vote_list_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Like like = likes.get(position);
        if (like != null) {

            ImageLoader.getInstance().displayImage(like.getPhoto().getMaxResolutionUrl(), viewHolder.imageView);

            User whom = like.getWhom();

            viewHolder.name.setText(whom.getFirst_name() + ", "+ UserInfoUtils.getFullAgesString(whom.getAge()));

            viewHolder.date.setText(DateUtils.fromDatabaseDate(whom.getUpdatedDate()));
        }
        return convertView;
    }

    public class ViewHolder {
        public final ImageView imageView;
        public final RobotoThinTextView name;
        public final TypefacedTextView date;
        public final View root;

        public ViewHolder(View root) {
            imageView = (ImageView) root.findViewById(R.id.imageView);
            name = (RobotoThinTextView) root.findViewById(R.id.name);
            date = (TypefacedTextView) root.findViewById(R.id.date);
            this.root = root;
        }
    }
}

