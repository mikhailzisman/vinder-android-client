package main.java.com.app.vinderapp.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 15.06.2014
 * Time: 22:52
 */
public class DateUtils {

    private static SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    private static SimpleDateFormat sdfOut = new SimpleDateFormat("HH:mm");

    public static Date getDate(String dateString){
        try {
            if (dateString==null)return new Date();
            return  sdfIn.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
    }

    public static String getDateAsString(Calendar date){

        return date.get(Calendar.DAY_OF_MONTH)
                + " " +
                date.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
                + ", " +
                sdfOut.format(date.getTime());
    }

    public static String fromDatabaseDate(Date databaseDate){
        if (databaseDate==null) return "";
        Calendar calendar = (Calendar) Calendar.getInstance().clone();
        calendar.setTime(databaseDate);
        return  getDateAsString(calendar);
    }

    public static String toDatabaseDateString(Date date){

        return  sdfIn.format(date);
    }
}
