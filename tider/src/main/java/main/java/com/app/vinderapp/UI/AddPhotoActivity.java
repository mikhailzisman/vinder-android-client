package main.java.com.app.vinderapp.UI;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 28.05.2014
 * Time: 14:21
 */

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.OnClick;
import main.java.com.app.vinderapp.IOC.BootstrapFragmentActivity;
import main.java.com.app.vinderapp.Models.Photo;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.UserProvider;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.*;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKPhotoArray;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class AddPhotoActivity extends BootstrapFragmentActivity {

    @Inject
    RestManager restManager;

    @Inject
    UserProvider userProvider;

    @OnClick(R.id.save)
    public void onSave(){
        onBackPressed();
    }


    public static final int PHOTOS_RESULT_CODE = 2;
    private AddPhotoActivity activity;
    private AddPhotoAdapter dataAdapter;
    private ArrayList<CheckedPhoto> checkedPhotos = new ArrayList<CheckedPhoto>();
    private ArrayList<Photo> alreadyAddedPhotoList = new ArrayList<Photo>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_photo_activity);

        activity = this;

        dataAdapter = new AddPhotoAdapter(this,
                R.layout.photo_addlist_item, new ArrayList<CheckedPhoto>());


        setListView();

        startLoadPhotos();

    }
    private void startLoadPhotos() {

        VKParameters parameters = new VKParameters();

        parameters.put("owner_id", VKSdk.getAccessToken().userId);

        VKRequest request = VKApi.getPhotosFromProfileRequest(parameters);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                VKPhotoArray photoAlbum = (VKPhotoArray) response.parsedModel;

                dataAdapter.clear();
                checkedPhotos = new ArrayList<CheckedPhoto>();
                for (VKApiPhoto photo : photoAlbum) {
                    checkedPhotos.add(new CheckedPhoto(photo));

                }

                dataAdapter = new AddPhotoAdapter(activity, 0, checkedPhotos);
                setListView();
                dataAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(VKError error) {
                Toast.makeText(activity, "Ошибка загрузки", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void setListView() {

        ListView listView = (ListView) findViewById(R.id.listView1);

        listView.setAdapter(dataAdapter);

    }


    private class AddPhotoAdapter extends ArrayAdapter<CheckedPhoto> {

        private List<CheckedPhoto> vkApiPhotos;

        public AddPhotoAdapter(Context context, int resource, List<CheckedPhoto> objects) {
            super(context, resource, objects);

            this.vkApiPhotos = objects;
        }


        private class ViewHolder {
            ImageView photo;
            CheckBox checkBox;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.photo_addlist_item, null);

                holder = new ViewHolder();
                holder.photo = (ImageView) convertView.findViewById(R.id.photo);
                holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

                String photo_604 = vkApiPhotos.get(position).photo.photo_604;
                ImageLoader.getInstance().displayImage(photo_604, holder.photo);


                holder.checkBox.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        CheckedPhoto checkedPhoto = (CheckedPhoto) cb.getTag();

                        checkedPhoto.checked = cb.isChecked();

                        Photo photo = new Photo();

                        photo.url75 = checkedPhoto.photo.photo_75;
                        photo.url130 = checkedPhoto.photo.photo_130;
                        photo.url604 = checkedPhoto.photo.photo_604;
                        photo.url807 = checkedPhoto.photo.photo_807;
                        photo.url1280 = checkedPhoto.photo.photo_1280;
                        photo.url2560 = checkedPhoto.photo.photo_2560;

                        restManager.vinderService.createPhoto(userProvider.load().getID(),photo,new Callback<Photo>() {
                            @Override
                            public void success(Photo photo, Response response) {
                                Toast.makeText(activity,"Фото добавлено",Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void failure(RetrofitError retrofitError) {
                                Toast.makeText(activity,"Ошибка при добавлении фото",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            CheckedPhoto checkedPhoto = vkApiPhotos.get(position);
            holder.checkBox.setChecked(checkedPhoto.checked);
            holder.checkBox.setTag(checkedPhoto);

            return convertView;
        }
    }


    private class CheckedPhoto {

        public boolean checked = false;
        public VKApiPhoto photo;

        private CheckedPhoto(VKApiPhoto photo) {
            this.photo = photo;
        }

    }
}
