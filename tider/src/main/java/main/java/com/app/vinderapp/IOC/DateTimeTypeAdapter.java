package main.java.com.app.vinderapp.IOC;

import com.google.gson.*;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.lang.reflect.Type;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 26.08.2014
 * Time: 1:40
 */
public class DateTimeTypeAdapter implements JsonSerializer<DateTime>,
        JsonDeserializer<DateTime> {
    @Override
    public DateTime deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
        DateTime parse = DateTime.parse(json.getAsString());

        return parse;
    }

    @Override
    public JsonElement serialize(DateTime src, Type typeOfSrc,
                                 JsonSerializationContext context) {
        JsonPrimitive jsonPrimitive = new JsonPrimitive(ISODateTimeFormat
                .dateTimeNoMillis()
                .print(src));

        return jsonPrimitive;
    }
}
