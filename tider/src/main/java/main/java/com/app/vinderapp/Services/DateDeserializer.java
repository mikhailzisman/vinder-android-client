package main.java.com.app.vinderapp.Services;

import com.google.gson.*;
import org.joda.time.DateTime;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 26.08.2014
 * Time: 0:31
 */
public class DateDeserializer
        implements JsonDeserializer<Date> {

    @Override
    public Date deserialize(JsonElement json, Type type,
                                             JsonDeserializationContext context) throws JsonParseException {

        DateTime dateTime = new Gson().fromJson(json, DateTime.class);

        return new Date(dateTime.getMillis());
    }
}
