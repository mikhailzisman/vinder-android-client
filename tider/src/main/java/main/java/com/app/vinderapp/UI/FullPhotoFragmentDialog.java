package main.java.com.app.vinderapp.UI;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.ButterKnife;
import butterknife.InjectView;
import main.java.com.app.vinderapp.Models.Photo;
import main.java.com.app.vinderapp.R;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.06.2014
 * Time: 11:15
 */
public class FullPhotoFragmentDialog extends DialogFragment {


    @InjectView(R.id.imageView)
    ImageView mImageView;
    @InjectView(R.id.layout)
    RelativeLayout mLayout;

    private Photo photo = null;

    public static void showDialog(FragmentManager fragmentManager, Photo photo) {

        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }

        DialogFragment newFragment = newInstance(photo);
        newFragment.show(ft,"dialog");
    }

    private static FullPhotoFragmentDialog newInstance(Photo photo) {
        FullPhotoFragmentDialog f = new FullPhotoFragmentDialog();

        Bundle args = new Bundle();
        args.putSerializable("photo", photo);

        f.setArguments(args);

        return f;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        return super.onCreateDialog(savedInstanceState);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getArguments().containsKey("photo"))
            photo = (Photo) getArguments().getSerializable("photo");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));

        View view = inflater.inflate(R.layout.full_screen_photo, null, false);

        ButterKnife.inject(this, view);

        mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        ImageLoader.getInstance().displayImage(photo.getMaxResolutionUrl(), mImageView);

        return view;
    }
}
