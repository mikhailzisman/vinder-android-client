package main.java.com.app.vinderapp.UI;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import butterknife.InjectView;
import butterknife.OnClick;
import main.java.com.app.vinderapp.Config;
import main.java.com.app.vinderapp.Constants;
import main.java.com.app.vinderapp.IOC.BootstrapFragmentActivity;
import main.java.com.app.vinderapp.Models.Like;
import main.java.com.app.vinderapp.Models.Settings;
import main.java.com.app.vinderapp.Models.User;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.UserProvider;
import main.java.com.app.vinderapp.billing.IabHelper;
import main.java.com.app.vinderapp.billing.IabResult;
import main.java.com.app.vinderapp.billing.Inventory;
import main.java.com.app.vinderapp.billing.Purchase;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.05.2014
 * Time: 10:42
 */
public class VoteHistoryActivity extends BootstrapFragmentActivity implements IabHelper.OnIabSetupFinishedListener, IabHelper.OnIabPurchaseFinishedListener {

    public static final String HISTORY_LIKE = "like";

    public static final int HISTORY_LIKE_RESULT_CODE = 2;
    private static final int BUY_VIP_PROCESS = 1;
    @Inject
    RestManager restManager;

    @Inject
    UserProvider userProvider;

    @InjectView(R.id.proPanel)
    LinearLayout mProPanel;
    private VoteHistoryActivity activity;

    @OnClick(R.id.goto_vote_screen)
    public void gotoVoteScreen() {
        finish();
    }

    @InjectView(R.id.notLikesFoundLayout)
    LinearLayout mNotLikesFoundLayout;

    @OnClick(R.id.buyVip)
    public void buyVIP() {
        buyVipProcess();
    }

    private ArrayList<Like> likes;
    private LikeListAdapter likesListAdapter;
    private ListView voteHistorylistView;
    private IabHelper iabHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        setContentView(R.layout.vote_history);

        if (Constants.IAB_ENABLED) {
            iabHelper = new IabHelper(this, Config.in_app_key);

            iabHelper.startSetup(this);
        }

        voteHistorylistView = (ListView) findViewById(R.id.listView);

        likes = new ArrayList<Like>();


        likesListAdapter = new LikeListAdapter(this, 0, likes);
        voteHistorylistView.setAdapter(likesListAdapter);


        likesListAdapter.notifyDataSetChanged();
        voteHistorylistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                Like likeAtPosition = (Like) adapterView.getItemAtPosition(position);

                Intent intent = new Intent();

                setResult(HISTORY_LIKE_RESULT_CODE, intent.putExtra(HISTORY_LIKE, likeAtPosition));

                setIntent(intent);

                finish();
            }
        });
    }

    private void updateLikesHistoryList() {

        restManager.vinderService.getDislikedPhotos(userProvider.load().getID(), new Callback<ArrayList<Like>>() {
            @Override
            public void success(ArrayList<Like> likeArray, Response response) {

                mNotLikesFoundLayout.setVisibility(likeArray.isEmpty() ? View.VISIBLE : View.GONE);
                voteHistorylistView.setVisibility(likeArray.isEmpty() ? View.GONE : View.VISIBLE);
                likes.clear();

                likes.addAll(likeArray);

                Collections.sort(likes);

                likesListAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError retrofitError) {

            }
        });
    }

    private void buyVipProcess() {
        try {
            iabHelper.launchPurchaseFlow(this, "vip", BUY_VIP_PROCESS, this);
        } catch (IllegalStateException ex) {
            Toast.makeText(this, "Сейчас покупка не доступна. Попробуйте немного позже", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        updateLikesHistoryList();

    }

    @Override
    public void onIabSetupFinished(IabResult result) {
        if (!result.isSuccess()) {

            Toast.makeText(this, "Проблема подключения к Android Market", Toast.LENGTH_LONG).show();
        }

        askForPurchase();
    }


    private void askForPurchase() {
        iabHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                Purchase vip = inv.getPurchase("vip");


                if (vip != null) {
                    mProPanel.setVisibility(View.GONE);

                    User user = userProvider.load();

                    Map<String, String> map = new HashMap<String, String>();

                    map.put("vip_status", "1");
                    user.vip_status = true;

                    userProvider.save(user);

                    restManager.vinderService.updateSettings(user.getID(),
                            user.getSettings(), new Callback<Settings>() {
                                @Override
                                public void success(Settings settings, Response response) {
                                    Toast.makeText(activity, "Вип обновлен", Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void failure(RetrofitError retrofitError) {

                                }
                            });
                }

                updateLikesHistoryList();
            }
        });
    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase info) {
        if (result.isSuccess()) {

            Toast.makeText(this, "Поздравляем, вы купили статус VIP", Toast.LENGTH_LONG).show();

            askForPurchase();

            updateUserStatusFromPurchase(info);

            updateLikesHistoryList();
        }
    }

    private void updateUserStatusFromPurchase(Purchase info) {


        if (info == null) return;

        String sku = info.getSku();

        if (!sku.equalsIgnoreCase("vip")) return;

        User user = userProvider.load();

        if (sku.equalsIgnoreCase("vip")) {
            user.setVip_status(true);
        }

        userProvider.save(user);

        restManager.vinderService.updateSettings(user.getID(), user.getSettings(),
                new Callback<Settings>() {
                    @Override
                    public void success(Settings settings, Response response) {

                        Toast.makeText(activity, "Вип обновлен", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {

                    }
                });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Constants.IAB_ENABLED)
            iabHelper.dispose();
    }

}
