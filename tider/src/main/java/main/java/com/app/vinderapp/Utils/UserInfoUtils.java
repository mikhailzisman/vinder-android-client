package main.java.com.app.vinderapp.Utils;

import com.vk.sdk.api.model.VKApiUserFull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 03.06.2014
 * Time: 11:32
 */
public class UserInfoUtils {

    public static String getName(VKApiUserFull vkApiUserFull) {
        if (vkApiUserFull != null)
            return vkApiUserFull.first_name;
        else return "";
    }

    static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public static String getAgesAsString(String bdate) {
            try {
                Date bday = simpleDateFormat.parse(bdate);

                int diffYears = getDiffYears(bday, new Date());

                return String.valueOf(diffYears);// + " " + getAgesTitle(diffYears);
            } catch (Exception e) {
                e.printStackTrace();
                return "0";
            }
    }

    public static Integer getAgesAsInt(String bdate) {
        try {
            return Integer.valueOf(getAgesAsString(bdate));
        } catch (NumberFormatException ex){
            return 0;
        }
    }

    public static String getFullAgesString(Integer ages){
        if (ages==null) return "";
        if (ages==0) return  "";
        return String.valueOf(ages) + " " + getAgesTitle(ages);
    }

    private static String getAgesTitle(int diffYears) {

        int div = diffYears / 10;
        int mod = diffYears % 10;

        if ((div == 0) || (div > 1)) {
            if (mod == 1) return "год";
            if ((mod > 2) && (mod <= 4)) return "года";
            return "лет";
        } else {
            return "лет";
        }
    }


    private static int getDiffYears(Date first, Date last) {
        Calendar a = getCalendar(first);
        Calendar b = getCalendar(last);
        int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
        if (a.get(Calendar.MONTH) > b.get(Calendar.MONTH) ||
                (a.get(Calendar.MONTH) == b.get(Calendar.MONTH) && a.get(Calendar.DATE) > b.get(Calendar.DATE))) {
            diff--;
        }
        return diff;
    }

    private static Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.US);
        cal.setTime(date);
        return cal;
    }

    public static String getFriendsConut(VKApiUserFull vkApiUserFull) {

        if ((vkApiUserFull == null) || (vkApiUserFull.counters == null)) return "0";

        return String.valueOf(vkApiUserFull.counters.friends);

    }

}
