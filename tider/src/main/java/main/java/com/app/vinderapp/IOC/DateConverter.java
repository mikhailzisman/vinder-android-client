package main.java.com.app.vinderapp.IOC;

import main.java.com.app.vinderapp.Models.User;
import com.google.gson.*;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 28.08.2014
 * Time: 2:36
 */
public class DateConverter implements JsonSerializer<Date>,
        JsonDeserializer<Date> {

    public static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");


    @Override
    public Date deserialize(JsonElement json, Type typeOfT,
                            JsonDeserializationContext context) throws JsonParseException {

        try {
            String asString = json.getAsString();
            return dateFormat.parse(asString);
        } catch (ParseException e) {
            e.printStackTrace();
            //return null;
        }

        int i1 = 0;

        i1 = 3;
        System.out.print(i1);


        return null;
    }

    @Override
    public JsonElement serialize(Date src, Type typeOfSrc,
                                 JsonSerializationContext context) {
        JsonPrimitive jsonPrimitive = new JsonPrimitive(dateFormat.format(src));

        return jsonPrimitive;
    }
}
