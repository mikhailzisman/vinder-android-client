package main.java.com.app.vinderapp.UI;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import main.java.com.app.vinderapp.R;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 02.06.2014
 * Time: 9:20
 */
public class DropDownMenuAdapter extends BaseAdapter implements SpinnerAdapter {

    final private int menu_length = 5+1;
    private final String[] menuArray;
    Context context;

    DropDownMenuAdapter(Context ctx) {
        context = ctx;

        menuArray = context.getResources().getStringArray(R.array.action_list);
    }

    @Override
    public View getDropDownView(int position, View view, ViewGroup viewGroup) {

        if (getItemViewType(position) == 0) {
            view = new View(context);
        }

        if (getItemViewType(position) == 1) {

            view = View.inflate(context, android.R.layout.simple_spinner_item, null);
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setTextSize(20);
            textView.setPadding(20, 20, 20, 20);
            textView.setText(menuArray[position - 1]);

            view = textView;
        }

        return view;

    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public int getCount() {
        return menu_length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = new View(context);
        if ((getItemViewType(position) == 0)||(position==1)) {
            view = View.inflate(context, R.layout.logo_layout, null);
        }
        return view;
    }

    @Override
    public int getItemViewType(int pos) {
        if (pos == 0)
            return 0;
        else
            return 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }
}
