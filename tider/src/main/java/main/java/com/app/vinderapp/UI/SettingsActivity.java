package main.java.com.app.vinderapp.UI;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import main.java.com.app.vinderapp.IOC.BootstrapFragmentActivity;
import main.java.com.app.vinderapp.Models.Settings;
import main.java.com.app.vinderapp.Models.User;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.AccessPreferences;
import main.java.com.app.vinderapp.Utils.UserProvider;
import main.java.com.app.vinderapp.Widget.RobotoThinTextView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 07.05.14
 * Time: 11:02
 */
public class SettingsActivity extends BootstrapFragmentActivity {

    public static final String USER_SETTINGS_IS_DEFINED = "USER_SETTINGS_IS_DEFINED";
    @InjectView(R.id.search)
    Button mSave;
    @InjectView(R.id.distance_seek_bar)
    SeekBar mDistanceSeekBar;
    @InjectView(R.id.range_seek_bar)
    FrameLayout mRangeSeekBar;
    @InjectView(R.id.filterCheckBox)
    CheckBox mFilterCheckBox;
    @InjectView(R.id.sex)
    Switch mSex;
    @InjectView(R.id.distanceMax)
    RobotoThinTextView mDistanceMax;
    @InjectView(R.id.distanceMin)
    RobotoThinTextView mDistanceMin;
    @InjectView(R.id.ageMax)
    RobotoThinTextView mAgeMax;
    @InjectView(R.id.ageMin)
    RobotoThinTextView mAgeMin;
    private Button save;

    @InjectView(R.id.filterSwitcher)
    Switch filterSwitcher;

    @Inject
    RestManager restManager;

    @Inject
    UserProvider userProvider;

    private RangeSeekBar<Integer> seekBar;
    private Settings curUserSettings;
    private SettingsActivity activity;


    @OnCheckedChanged(R.id.filterCheckBox)
    void onChecked(boolean checked) {
        filterSwitcher.setEnabled(checked);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_layout);

        activity = this;
        User user = userProvider.load();

        curUserSettings = user.getSettings() != null ?
                user.getSettings() :
                new Settings();

        seekBar = new RangeSeekBar<Integer>(18, 75, this);
        seekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) {
                curUserSettings.setMin_age(minValue);
                curUserSettings.setMax_age(maxValue);

                mAgeMin.setText(String.valueOf(minValue));
                mAgeMax.setText(String.valueOf(maxValue));

            }
        });


        mDistanceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int km, boolean b) {
                curUserSettings.setRange_in_km(km);
                mDistanceMax.setText(String.valueOf(km) + " км");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mFilterCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                curUserSettings.setFilter_by_pro(b);
                filterSwitcher.setEnabled(b);
            }
        });

        filterSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                curUserSettings.setFilter_is_pro(b);
            }
        });

        mSex.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                curUserSettings.setSex(b ? 1 : 2);
            }
        });

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

// add RangeSeekBar to pre-defined layout
        ViewGroup layout = (ViewGroup) findViewById(R.id.range_seek_bar);
        layout.addView(seekBar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void init() {
        seekBar.setSelectedMinValue(curUserSettings.getMin_age());

        seekBar.setSelectedMaxValue(curUserSettings.getMax_age());

        mDistanceSeekBar.setProgress(curUserSettings.getRange_in_km());

        mAgeMin.setText(String.valueOf(curUserSettings.getMin_age()));

        mAgeMax.setText(String.valueOf(curUserSettings.getMax_age()));

        mDistanceMax.setText(String.valueOf(curUserSettings.getRange_in_km()) + "км");

        mDistanceSeekBar.setProgress(curUserSettings.getRange_in_km());

        mFilterCheckBox.setChecked(curUserSettings.getFilter_by_pro());
        filterSwitcher.setEnabled(curUserSettings.getFilter_by_pro());

        filterSwitcher.setChecked(curUserSettings.getFilter_is_pro());

        //если мужик , true- то ставим в левое положение
        mSex.setChecked(curUserSettings.getSex() == 1);
    }


    private void updateSettings() {


        restManager.vinderService.updateSettings(userProvider.load().getID(), curUserSettings, new Callback<Settings>() {
            @Override
            public void success(Settings settings, Response response) {
                User user = userProvider.load();
                user.setSettings(settings);
                userProvider.save(user);


                Toast.makeText(activity, "Настройки обновлены", Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(activity, "Ошибка обновления настроек", Toast.LENGTH_LONG).show();
            }
        });

    }


    @Override
    protected void onPause() {

        updateSettings();

        AccessPreferences.commit(this, USER_SETTINGS_IS_DEFINED, true);

        super.onPause();
    }

}
