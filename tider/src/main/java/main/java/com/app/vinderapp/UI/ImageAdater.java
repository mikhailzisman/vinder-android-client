package main.java.com.app.vinderapp.UI;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import main.java.com.app.vinderapp.Models.Photo;
import main.java.com.app.vinderapp.R;
import com.viewpagerindicator.IconPagerAdapter;

import java.util.ArrayList;

public class ImageAdater extends FragmentPagerAdapter implements IconPagerAdapter {

    ArrayList<Photo> photos = new ArrayList<Photo>();

    public ImageAdater(FragmentManager fm, ArrayList<Photo> photos) {
        super(fm);

        this.photos = photos;
    }

    @Override
    public Fragment getItem(int position) {
        return ImageFragment.newInstance(photos.get(position));
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    @Override
    public int getIconResId(int index) {
        return R.drawable.ava;
    }

}
