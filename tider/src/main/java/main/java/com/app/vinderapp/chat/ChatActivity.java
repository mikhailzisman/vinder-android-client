package main.java.com.app.vinderapp.chat;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.05.14
 * Time: 10:56
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import main.java.com.app.vinderapp.IOC.BootstrapFragmentActivity;
import main.java.com.app.vinderapp.Models.Friendship;
import main.java.com.app.vinderapp.Models.Message;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.UserProvider;
import com.google.common.eventbus.Subscribe;
import com.squareup.otto.Bus;
import com.vk.sdk.VKSdk;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class ChatActivity extends BootstrapFragmentActivity {
    public static final String FRIENDSHIP = "friendship";
    private DiscussArrayAdapter adapter;
    private ListView lv;
    private EditText editText1;

    @Inject
    UserProvider userProvider;

    @Inject
    Bus BUS;

    @Inject
    RestManager restManager;
    private ChatActivity activity;
    private Friendship friendship;
    private BroadcastReceiver mMessageReciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Message  message = (Message) intent.getSerializableExtra("message");

            onMessageRevieved(message);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        setContentView(R.layout.activity_discuss);

        findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                send();
            }
        });

        lv = (ListView) findViewById(R.id.listView1);


        editText1 = (EditText) findViewById(R.id.editText1);
        editText1.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    send();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        //BUS.register(this);

        friendship = (Friendship) getIntent().getSerializableExtra(FRIENDSHIP);


        restManager.vinderService.findMessageFromFrendship(friendship, new Callback<ArrayList<Message>>() {
            @Override
            public void success(ArrayList<Message> messages, Response response) {

                Collections.sort(messages);

                adapter = new DiscussArrayAdapter(activity, R.layout.listitem_discuss, friendship);

                lv.setAdapter(adapter);

                adapter.addAll(messages);

                adapter.notifyDataSetChanged();

                scrollMyListViewToBottom();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(activity, "Ошибка чтения сообщений", Toast.LENGTH_LONG).show();
            }
        });


        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReciever, new IntentFilter("message"));
    }

    private void send() {

        String textMessage = editText1.getText().toString();

        if (textMessage.trim().equalsIgnoreCase("")) return;

        Message message = new Message();

        message.setWho(friendship.getMe());
        message.setWhom(friendship.getHim());
        message.setMessage(textMessage);
        message.setCreatedAt(new Date());


        restManager.vinderService.createMessage(message, new Callback<Message>() {
            @Override
            public void success(Message message, Response response) {}

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(activity, "Ошибка при отправке сообщения", Toast.LENGTH_LONG).show();
            }
        });

        editText1.setText("");
    }

    private boolean IsMessageFromMe(Message message) {
        return message.getWho().getUid().toString().equals(VKSdk.getAccessToken().userId);
    }

    private boolean IsMessageToMe(Message message) {
        return message.getWhom().getUid().toString().equals(VKSdk.getAccessToken().userId);
    }

    private void scrollMyListViewToBottom() {
        lv.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                lv.setSelection(adapter.getCount() - 1);
            }
        });
    }

    public void onMessageRevieved(Message message) {

        if (!IsMessageToMe(message)&&
                !IsMessageFromMe(message))
            return;

        if (IsMessageToMe(message)) {
            Toast.makeText(activity, "Вам сообщение", Toast.LENGTH_LONG).show();
        }

        if (IsMessageFromMe(message)){
            Toast.makeText(activity, "Сообщение отправлено", Toast.LENGTH_LONG).show();
        }

        if (adapter == null)
            adapter = new DiscussArrayAdapter(activity, R.layout.listitem_discuss, friendship);

        adapter.add(message);

        adapter.notifyDataSetChanged();

        scrollMyListViewToBottom();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReciever);

        BUS.unregister(this);
        super.onPause();
    }
}
