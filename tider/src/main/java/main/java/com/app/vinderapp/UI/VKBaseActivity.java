package main.java.com.app.vinderapp.UI;

import android.app.Activity;
import android.content.Intent;
import com.vk.sdk.VKUIHelper;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.05.14
 * Time: 10:00
 */
public abstract class VKBaseActivity extends Activity{
    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        VKUIHelper.onActivityResult(requestCode, resultCode, data);
    }
}
