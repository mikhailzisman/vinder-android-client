package main.java.com.app.vinderapp.UI;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.05.14
 * Time: 10:11
 */
public interface ActivitySwipeInterface {
    public void onSwipeLeft();
    public void onSwipeRight();
    public void onSwipeDown();
    public void onSwipeUp();
}
