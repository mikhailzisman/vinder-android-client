package main.java.com.app.vinderapp.UI;

import android.content.Context;
import android.graphics.*;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.SeekBar;
import main.java.com.app.vinderapp.R;

public class SeekBarLabeled extends SeekBar{

    private OnSeekBarChangeListener mOnSeekBarChangeListener;

    private Drawable progressDrawable;
    private Rect barBounds, labelTextRect;
    private Bitmap labelBackground;
    private Point labelPos;
    private Paint labelTextPaint, labelBackgroundPaint;

    int viewWidth, barHeight, labelOffset;
    //    private int thumbX;
    float progressPosX;
    private String expression;

    public SeekBarLabeled(Context context) {
        super(context);
        // Log.i("Seekbar", "DemoSeek");
        progressDrawable = getProgressDrawable();

        // labelBackground = BitmapFactory.decodeResource(getResources(),
        // R.drawable.thumb_marker);

        labelBackground = drawableToBitmap(getResources().getDrawable(R.drawable.slider));

        labelTextPaint = new Paint();
        labelTextPaint.setColor(Color.WHITE);
        labelTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
        labelTextPaint.setAntiAlias(true);
        labelTextPaint.setDither(true);
        labelTextPaint.setTextSize(13f);

        labelBackgroundPaint = new Paint();

        barBounds = new Rect();
        labelTextRect = new Rect();

        labelPos = new Point();

    }

    public Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }


    public SeekBarLabeled(Context context, AttributeSet attrs) {
        super(context, attrs);

        progressDrawable = getProgressDrawable();

        labelBackground = BitmapFactory.decodeResource(getResources(),
                R.drawable.slider);

        labelTextPaint = new Paint();
        labelTextPaint.setColor(Color.WHITE);
        labelTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
        labelTextPaint.setAntiAlias(true);
        labelTextPaint.setDither(true);
        labelTextPaint.setTextSize(15f);

        labelBackgroundPaint = new Paint();

        barBounds = new Rect();
        labelTextRect = new Rect();

        labelPos = new Point();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        // Log.i("Seekbar", "onMeasure");
        if (labelBackground != null) {

            viewWidth = getMeasuredWidth();
            barHeight = getMeasuredHeight() - getPaddingTop()
                    - getPaddingBottom();
            setMeasuredDimension(viewWidth + labelBackground.getWidth(),
                    barHeight + labelBackground.getHeight() / 2);
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();
        if (labelBackground != null) {
            barBounds.left = getPaddingLeft();
            barBounds.top = (int) (labelBackground.getHeight() / 2f);
            barBounds.right = barBounds.left + viewWidth - getPaddingRight()
                    - getPaddingLeft();
            barBounds.bottom = barBounds.top + barHeight - getPaddingBottom()
                    - getPaddingTop();

            progressPosX = barBounds.top
                    + ((float) this.getProgress() / (float) this.getMax())
                    * barBounds.height() + getTopPaddingOffset();

            labelPos.y = getBottom() - (int) progressPosX - labelOffset
                    + (int) (getProgress() * 0.1f);
            labelPos.x = getPaddingLeft();

            progressDrawable = getProgressDrawable();

            progressDrawable.setBounds(barBounds.left, barBounds.top,
                    barBounds.right, getBottom());

            progressDrawable.draw(canvas);

            String pro = getProgress() * multiplier + "";
            if (expression != null) {
                pro = pro.concat(expression);
            }
            labelTextPaint.getTextBounds(pro, 0, pro.length(), labelTextRect);

            canvas.drawBitmap(labelBackground, labelPos.x, labelPos.y,
                    labelBackgroundPaint);

            canvas.drawText(pro, labelPos.x + labelBackground.getWidth() / 2
                    - labelTextRect.width() / 2 + 15, labelPos.y
                    + labelBackground.getHeight() / 2 + labelTextRect.height()
                    / 2 - 5, labelTextPaint);

        }
        canvas.restore();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        invalidate();
        return super.onTouchEvent(event);

    }




    private int multiplier = 1;

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setExpression(String expression) {
        this.expression = expression.trim();
    }

    public String getExpression() {
        return expression;
    }

}
