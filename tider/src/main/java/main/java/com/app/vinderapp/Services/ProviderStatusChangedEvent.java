package main.java.com.app.vinderapp.Services;

import android.os.Bundle;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 12.08.2014
 * Time: 9:23
 */
public class ProviderStatusChangedEvent {
    public final String provider;
    public final int status;
    public final Bundle extras;

    public ProviderStatusChangedEvent(String provider, int status, Bundle extras) {
        this.provider = provider;
        this.status = status;
        this.extras = extras;
    }
}
