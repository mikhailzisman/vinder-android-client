package main.java.com.app.vinderapp.Models;

import java.io.Serializable;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 10.08.2014
 * Time: 13:35
 */
public class Photo implements Serializable{



    public Long ID;

    public User user;


    public String url75 = "";
    public String url130 = "";
    public String url604 = "";
    public String url807 = "";
    public String url1280 = "";
    public String url2560 = "";



    public SortedMap<Integer, String> getResolutions() {
        SortedMap<Integer, String> urls = new TreeMap<Integer, String>();
        int i = 0;
        if ((url75!=null)&&!url75.equalsIgnoreCase(""))
            urls.put(i++, url75);

        if ((url130!=null)&&!url130.equalsIgnoreCase(""))
            urls.put(i++, url130);

        if ((url604!=null)&&!url604.equalsIgnoreCase(""))
            urls.put(i++, url604);

        if ((url807!=null)&&!url807.equalsIgnoreCase(""))
            urls.put(i++, url807);

        if ((url1280!=null)&&!url1280.equalsIgnoreCase(""))
            urls.put(i++, url1280);

        if ((url2560!=null)&&!url2560.equalsIgnoreCase(""))
            urls.put(i++, url2560);

        return urls;
    }

    public String getMaxResolutionUrl() {
        Integer lastKey = getResolutions().lastKey();

        return getResolutions().get(lastKey);
    }

    public Long getId() {
        return ID;
    }

    public void setId(Long id) {
        this.ID = id;
    }
}
