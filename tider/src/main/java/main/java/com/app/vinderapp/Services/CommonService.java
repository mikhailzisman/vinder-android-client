package main.java.com.app.vinderapp.Services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import main.java.com.app.vinderapp.Config;
import main.java.com.app.vinderapp.IOC.Injector;
import com.google.gson.Gson;

import javax.inject.Inject;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Mikhail on 08.12.13.
 */
public class CommonService extends Service{


    @Inject
    protected Gson gson;

    private Class<? extends Service>[] servicesFromMonitoring = new Class[]{
            SocketService.class,
            GPSService.class,
    };

    @Override
    public void onCreate() {
        super.onCreate();

        Injector.inject(this);

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    ///////////////////////////////////////////////////////////////

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                checkAndRelaunchServices();
            }
        },100, Config.check_service_period);


        return START_STICKY;
    }


    private void checkAndRelaunchServices() {
        for (Class<? extends Service> service : servicesFromMonitoring) {
            if (!isServiceRunning(service))
                startService(new Intent(this, service));
        }
    }

    private boolean isServiceRunning(Class<? extends Service> service) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo services : manager.getRunningServices(Integer.MAX_VALUE)) {
            String className = services.service.getClassName();
            if (service.getName().equals(className)) {
                return true;
            }
        }

        return false;
    }


}
