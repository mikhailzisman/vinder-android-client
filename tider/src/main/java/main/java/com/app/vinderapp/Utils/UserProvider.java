package main.java.com.app.vinderapp.Utils;

import android.content.Context;
import main.java.com.app.vinderapp.Constants;
import main.java.com.app.vinderapp.IOC.Injector;
import main.java.com.app.vinderapp.Models.User;
import com.google.gson.Gson;
import com.vk.sdk.api.model.VKApiUserFull;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 01.06.2014
 * Time: 23:25
 */
public class UserProvider {

    @Inject
    Gson gson;

    @Inject
    Context context;

    public UserProvider() {
        Injector.inject(this);
    }

    public VKApiUserFull loadVKInfo() {
        String userAsJson = AccessPreferences.get(context, Constants.USER_VK_INFO_SHARED_PREFERENCE, "");
        if (userAsJson.equalsIgnoreCase(""))
            return null;
        VKApiUserFull obj = gson.fromJson(userAsJson, VKApiUserFull.class);
        return obj;
    }

    public void save(VKApiUserFull vkApiUserFull) {
        String userAsJson = gson.toJson(vkApiUserFull);
        AccessPreferences.commit(context, Constants.USER_VK_INFO_SHARED_PREFERENCE, userAsJson);
    }


    public User load() {
        String userAsJson = AccessPreferences.get(context, Constants.USER_ID_SHARED_PREFERENCE, "");
        if (userAsJson.equalsIgnoreCase(""))
            return null;
        User obj = gson.fromJson(userAsJson, User.class);
        return obj;
    }
    public void save(User user) {
        String userAsJson = gson.toJson(user);
        AccessPreferences.commit(context, Constants.USER_ID_SHARED_PREFERENCE, userAsJson);
    }
}
