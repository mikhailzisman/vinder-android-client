package main.java.com.app.vinderapp;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 30.04.14
 * Time: 11:01
 */
public class Config {
    public static boolean release = true;
    public final static String vk_id_app = "4336144";
    public static final long GPS_UPDATE_TIME_MILLIS = 1000 * 60 * 1;//1 mins
////    LOCALLY
//    public static String site = "http://10.0.3.2:9000";
//
//    public static String socketSite = "ws://10.0.3.2:9000";

//   SERVER
    public static String site = "http://protected-river-4613.herokuapp.com";

    public static String socketSite = "ws://protected-river-4613.herokuapp.com";
    public static long check_service_period = 1000 * 60; //1 min
    public static int maxPhotosToAddInUsualAccount = 5;
    public static int maxPhotosToAddInVIPAccount = 10;

    public static String in_app_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgsxXmVPDnUaFVfLTj1bqs+FD1YBckihjD1pSVZHPRmsguhM9V5Nzs8TzIl1wIGFDWuN/fmybqSI0Iy1+JwpsipYoW+PwWXEbvwSn1VJY+kr+xL3RnuWsJbutDt0LIpnaARUWnyx0mV11SWLGTVeVkpTiPmWaK1Z4EN0eMtYFV3hzwO6vMTUGtZP78SXipme+nNBEMu1B231izQw0JhFRoOdK6SOgoaR8bBnHsWFFMJnwWu2iR51Hz2pZHJaBQFe5pzvZWQdxr8lUAp7hMv5CUFf00Kxp/Ewf/csnAG/qloLAxaiEDNBYsfVRPOLBVTP4zotM3ZZ+fvz1zKZLT0RY1wIDAQAB";

}
