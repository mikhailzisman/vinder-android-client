package main.java.com.app.vinderapp.REST;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 30.05.2014
 * Time: 13:23
 */
public class ResponseRESTEvent<T> {
    public T response;

    public ResponseRESTEvent(T response) {
        this.response = response;
    }
}
