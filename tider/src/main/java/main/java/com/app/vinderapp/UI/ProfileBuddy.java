package main.java.com.app.vinderapp.UI;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;
import main.java.com.app.vinderapp.IOC.BootstrapFragmentActivity;
import main.java.com.app.vinderapp.Models.Photo;
import main.java.com.app.vinderapp.Models.User;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.UserInfoUtils;
import com.viewpagerindicator.CirclePageIndicator;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import org.json.JSONException;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 26.05.2014
 * Time: 11:26
 */
public class ProfileBuddy extends BootstrapFragmentActivity {


    @Inject
    RestManager restManager;

    ArrayList<Photo> photos = new ArrayList<Photo>();
    private ImageAdater imageAdater;

    @InjectView(R.id.name)
    TextView name;

    @InjectView(R.id.friends)
    TextView friends;

    @InjectView(R.id.distance)
    TextView dsitance;
    private ProfileBuddy activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        setContentView(R.layout.profile_buddy_layout);

        ButterKnife.inject(this);

        User user = (User) getIntent().getSerializableExtra("user");

        name.setText(user.getFirst_name() + "," + UserInfoUtils.getFullAgesString(user.getAge()));

        startGetMutualFriendsCount(user.getUid());


        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        imageAdater = new ImageAdater(getSupportFragmentManager(), photos);
        pager.setAdapter(imageAdater);
        final float density = getResources().getDisplayMetrics().density;
        CirclePageIndicator circeIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
        circeIndicator.setFillColor(0xFF000000);
        circeIndicator.setStrokeColor(0xFF000000);
        circeIndicator.setStrokeWidth(1);
        circeIndicator.setRadius(4 * density);
        circeIndicator.setViewPager(pager);

        circeIndicator.setViewPager(pager);

        restManager.vinderService.getPhotosByUserId(user.getID(), new Callback<ArrayList<Photo>>() {
            @Override
            public void success(ArrayList<Photo> photosGot, Response response) {
                photos.clear();
                photos.addAll(photosGot);
                imageAdater.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(activity, "Ошибка при получении фото", Toast.LENGTH_LONG).show();
            }
        });
    }



    private void startGetMutualFriendsCount(final Long uid) {
        VKParameters vkParameters = new VKParameters();
        vkParameters.put("target_uid", uid);
        vkParameters.put("source_uid", VKSdk.getAccessToken().userId);
        VKRequest request = VKApi.friends().getMutual(vkParameters);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                int mutual_friends_count = 0;

                if (response.json.has("response"))
                    try {
                        mutual_friends_count = response.json.getJSONArray("response").length();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                friends.setVisibility(View.VISIBLE);
                friends.setText(String.valueOf(mutual_friends_count));
            }
        });


    }

}
