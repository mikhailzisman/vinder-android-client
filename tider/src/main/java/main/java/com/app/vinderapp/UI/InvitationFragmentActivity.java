package main.java.com.app.vinderapp.UI;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;
import butterknife.InjectView;
import butterknife.OnClick;
import main.java.com.app.vinderapp.IOC.BootstrapFragmentActivity;
import main.java.com.app.vinderapp.Models.Friendship;
import main.java.com.app.vinderapp.Models.Like;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.chat.ChatActivity;
import com.nostra13.universalimageloader.core.ImageLoader;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 16.06.2014
 * Time: 2:57
 */
public class InvitationFragmentActivity extends BootstrapFragmentActivity {


    public static final String INVITATION = "invitation";
    @InjectView(R.id.myImageView)
    ImageView mMyImageView;

    @InjectView(R.id.himImageView)
    ImageView mHimImageView;

    @Inject
    RestManager restManager;
    private InvitationFragmentActivity activity;

    public static int CONTINUE_REQUEST_CODE = 100;

    @OnClick(R.id.startChat)
    public void yes() {

        if (friendship == null)
            finish();

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.FRIENDSHIP, friendship);
        startActivity(intent);
    }

    @OnClick(R.id.contiue)
    public void no() {
        setResult(CONTINUE_REQUEST_CODE);
        finish();
    }

    private Friendship friendship = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        activity = this;

        setContentView(R.layout.add_friend_dialog);

        //INVITATION MUST BE HERE
        if (!getIntent().hasExtra(INVITATION)) finish();

        friendship = (Friendship) getIntent().getSerializableExtra(INVITATION);

        if (friendship == null) finish();

        //dislay
        if (friendship.getLikeToUser1() != null)
            showPhotoFromLike(friendship.getLikeToUser1(), mMyImageView);


        if (friendship.getLikeToUser2() != null)
            showPhotoFromLike(friendship.getLikeToUser2(), mHimImageView);

        friendship.setUser1Delivered(true);

        restManager.vinderService.updateFriendship(friendship, new Callback<Friendship>() {
            @Override
            public void success(Friendship friendship, Response response) {
                Toast.makeText(activity, "Результат обновлен", Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(activity, "Ошибка обновления", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showPhotoFromLike(Like like, final ImageView imageView) {
        ImageLoader.getInstance().displayImage(like.getPhoto().url604, imageView);
    }

}
