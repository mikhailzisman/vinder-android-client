package main.java.com.app.vinderapp.UI;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.05.2014
 * Time: 7:21
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import main.java.com.app.vinderapp.Models.Photo;
import com.nostra13.universalimageloader.core.ImageLoader;

public final class ImageFragment extends Fragment {

    public static ImageFragment newInstance(Photo photo) {
        ImageFragment fragment = new ImageFragment();

        fragment.photo = photo;

        return fragment;
    }

    private Photo photo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ImageView imageView = new ImageView(getActivity());
        imageView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        ImageLoader.getInstance().displayImage(photo.url604,imageView);

        LinearLayout layout = new LinearLayout(getActivity());
        layout.setTag(photo);
        layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        layout.setGravity(Gravity.CENTER);
        layout.addView(imageView);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!( view.getTag() instanceof Photo)) return;
                Photo photo = (Photo) view.getTag();
                FullPhotoFragmentDialog.showDialog(getFragmentManager(),photo);
            }
        });

        return layout;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!(view.getTag() instanceof Photo)) return;

        Photo photo = (Photo) view.getTag();

        FullPhotoFragmentDialog.showDialog(getFragmentManager(),photo);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
       // outState.putString(KEY_CONTENT, url);
    }
}
