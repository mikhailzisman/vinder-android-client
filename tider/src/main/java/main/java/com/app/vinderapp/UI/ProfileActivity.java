package main.java.com.app.vinderapp.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.InjectView;
import butterknife.OnClick;
import main.java.com.app.vinderapp.Config;
import main.java.com.app.vinderapp.Constants;
import main.java.com.app.vinderapp.IOC.BootstrapFragmentActivity;
import main.java.com.app.vinderapp.Models.Photo;
import main.java.com.app.vinderapp.Models.Settings;
import main.java.com.app.vinderapp.Models.User;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.UserInfoUtils;
import main.java.com.app.vinderapp.Utils.UserProvider;
import main.java.com.app.vinderapp.Widget.RobotoRegularTextView;
import main.java.com.app.vinderapp.Widget.RobotoThinTextView;
import main.java.com.app.vinderapp.billing.*;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;


import org.json.JSONException;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 07.05.14
 * Time: 10:39
 */
public class ProfileActivity extends BootstrapFragmentActivity implements IabHelper.OnIabSetupFinishedListener, IabHelper.OnIabPurchaseFinishedListener {

    private static final int IMAGE_VIEW_ID = 999;
    private static final int BUY_VIP_PROCESS = 1;
    private static final int BUY_PRO_PROCESS = 2;
    @Inject
    RestManager restManager;

    @Inject
    UserProvider userProvider;

    @InjectView(R.id.name)
    TextView tvName;

    @InjectView(R.id.friends)
    TextView tvFriends;

    private static final int REQUEST_ADD_PHOTO = 1;
    @InjectView(R.id.difference)
    RobotoRegularTextView mDifference;
    @InjectView(R.id.pro)

    RobotoThinTextView mPro;
    @InjectView(R.id.distance)
    RobotoThinTextView mDistance;
    @InjectView(R.id.hideAge)
    CheckBox mHideAge;
    @InjectView(R.id.hideAgeLayout)
    LinearLayout mHideAgeLayout;
    @InjectView(R.id.buyVip)
    Button mBuyVip;
    @InjectView(R.id.buyPro)
    Button mBuyPro;
    @InjectView(R.id.upProfile)
    LinearLayout mUpProfile;
    @InjectView(R.id.labelMorePhoto)
    TextView mLabelMorePhoto;
    private ProfileActivity activity;
    private IabHelper iabHelper;


    @OnClick(R.id.buyVip)
    public void buyVIP() {
        buyVipProcess();
    }

    private void buyVipProcess() {
        try {
            iabHelper.launchPurchaseFlow(this, "vip", BUY_VIP_PROCESS, this);
        } catch (IllegalStateException ex) {
            Toast.makeText(activity, "Сейчас покупка не доступна. Попробуйте немного позже", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.buyPro)
    public void buyPro() {
        buyProProcess();
    }

    private void buyProProcess() {
        try {
            iabHelper.launchPurchaseFlow(this, "pro", BUY_PRO_PROCESS, this);
        } catch (IllegalStateException ex) {
            Toast.makeText(activity, "Сейчас покупка не доступна. Попробуйте немного позже", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.upProfile)
    public void upProfile() {
        Toast.makeText(this, "Эта возможность появится в следующей версии", Toast.LENGTH_LONG).show();
    }


    @InjectView(R.id.tags)
    TextView tags;

    @OnClick(R.id.tags)
    public void editTags() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);

        final EditText input = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);

        adb.setMessage("Ваша характеристика");
        adb.setView(input);
        adb.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tags.setText(input.getText());
            }
        }).show();
    }

    private LinearLayout gallery;
    private QuickAction quickAction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.profile_layout);


        if (Constants.IAB_ENABLED) {

            iabHelper = new IabHelper(this, Config.in_app_key);
            iabHelper.startSetup(this);

        }

        activity = this;
        gallery = (LinearLayout) findViewById(R.id.mygallery);

        findViewById(R.id.addPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int photosCount = gallery.getChildCount();
                User user = userProvider.load();

                if ((photosCount >= Config.maxPhotosToAddInUsualAccount && !user.vip_status)) {
                    showBuyVipDialog("Купите VIP аккаунт, чтобы добавить больше фото");
                    return;
                }

                if ((photosCount >= Config.maxPhotosToAddInVIPAccount && user.vip_status)) {
                    Toast.makeText(activity, "Вы достигли максимального количества фотографий фотографий", Toast.LENGTH_LONG).show();
                    return;
                }

                showAddPhotoWindow();

            }


        });

        initUserData();


        quickAction = new QuickAction(this);

        mDifference.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quickAction.show(view);
            }
        });
    }

    public void onRetrofitError(RetrofitError retrofitError) {
        Toast.makeText(this, retrofitError.getResponse().getReason(), Toast.LENGTH_LONG).show();
    }

    private void initUserData() {

        tvName.setText(UserInfoUtils.getName(userProvider.loadVKInfo()) + " " + UserInfoUtils.getFullAgesString(userProvider.load().getAge()));

        //tvFriends.setText(UserInfoUtils.getFriendsConut(userProvider.loadVKInfo()));

        final User user = userProvider.load();

        mHideAge.setEnabled(user.vip_status);

        mHideAge.setChecked(user.getSettings().getHide_age());

        mHideAge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, final boolean checked) {
                User user = userProvider.load();
                Settings settings = user.getSettings();
                settings.setHide_age(checked);
                userProvider.save(user);

                restManager.vinderService.updateUser(userProvider.load().getID(), user, new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        Toast.makeText(activity, "Обновлено", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Toast.makeText(activity, "Ошибка обновления", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

        mHideAgeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!user.vip_status) {
                    showBuyVipDialog("Чтобы спрятать возраст купите VIP-аккаунт");
                }

            }
        });

    }

    private void showBuyVipDialog(String message) {
        final AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        adb.setMessage(message)
                .setCancelable(true)
                .setPositiveButton("Купить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        buyVipProcess();
                    }
                })
                .setNegativeButton("Нет, спасибо", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateAttachedPhotos();

        User user = userProvider.load();
        tvName.setText(user.getFirst_name() + "," + UserInfoUtils.getFullAgesString(user.getAge()));


        startGetMutualFriendsCount(userProvider.load().getUid());

        mPro.setVisibility(View.VISIBLE);
        if (user.pro_status) {
            mPro.setText("PRO");
            mPro.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.pro_icon_large), null, null, null);
        }

    }

    private void startGetMutualFriendsCount(final Long uid) {
        VKParameters vkParameters = new VKParameters();
        vkParameters.put("target_uid", uid);
        vkParameters.put("source_uid", VKSdk.getAccessToken().userId);
        VKRequest request = VKApi.friends().getMutual(vkParameters);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                int mutual_friends_count = 0;

                if (response.json.has("response"))
                    try {
                        mutual_friends_count = response.json.getJSONArray("response").length();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                tvFriends.setVisibility(View.VISIBLE);
                tvFriends.setText(String.valueOf(mutual_friends_count));
            }
        });
    }

    private void updateAttachedPhotos() {
        restManager.vinderService.getPhotosByUserId(userProvider.load().getID(), new Callback<ArrayList<Photo>>() {
            @Override
            public void success(ArrayList<Photo> photos, Response response) {
                gallery.removeAllViews();

                inflatewithPhotos(photos);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(activity, "Ошибка при получении фотографий", Toast.LENGTH_LONG).show();
            }

        });
    }

    private void inflatewithPhotos(ArrayList<Photo> photos) {
        for (Photo photo : photos) {
            LinearLayout imageViewPhoto = getImageViewPhoto();
            ImageLoader.getInstance().displayImage(photo.getMaxResolutionUrl(), (ImageView) imageViewPhoto.findViewById(IMAGE_VIEW_ID));

            View rootView = imageViewPhoto.getRootView();
            rootView.setTag(photo);
            rootView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    Photo photo = (Photo) view.getTag();

                    showDeletePhotoDialog(photo);
                    return true;
                }
            });

            gallery.addView(rootView);
        }
    }

    private void showDeletePhotoDialog(final Photo photo) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Удалить фото?")
                .setMessage("Удаление фото")
                .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        deletePhoto(photo);

                    }
                })
                .setNegativeButton("Нет", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).show();
    }

    private void deletePhoto(Photo photo) {
        restManager.vinderService.destroy(photo.ID, new Callback<Photo>() {
            @Override
            public void success(Photo photo, Response response) {
                if (photo != null) {
                    Toast.makeText(activity, "Фотография удалена", Toast.LENGTH_SHORT).show();
                    updateAttachedPhotos();
                }
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(activity, "Ошибка при удалении", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showAddPhotoWindow() {
        startActivity(new Intent(this, AddPhotoActivity.class));
    }

    LinearLayout getImageViewPhoto() {
        //Bitmap bm = decodeSampledBitmapFromUri(path, 220, 220);

        LinearLayout layout = new LinearLayout(getApplicationContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(5, 5, 5, 5);
        layout.setLayoutParams(layoutParams);
        layout.setGravity(Gravity.CENTER);

        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setId(IMAGE_VIEW_ID);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(250, 250));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        layout.addView(imageView);

        return layout;
    }

    @Override
    public void onIabSetupFinished(IabResult result) {
        if (!result.isSuccess()) {

            Toast.makeText(this, "Проблема подключения к Android Market", Toast.LENGTH_LONG).show();
        }

        askForPurchase();
    }

    private void askForPurchase() {
        iabHelper.queryInventoryAsync(new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                Purchase vip = inv.getPurchase("vip");
                Purchase pro = inv.getPurchase("pro");

                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("У вас ");

                if (vip != null) {

                    SkuDetails vipDetails = inv.getSkuDetails("vip");
                    stringBuilder.append(vipDetails.getDescription());

                    updateUserStatusFromPurchase(vip);
                    updateUIifVIP();

                }

                if (pro != null) {
                    SkuDetails vipDetails = inv.getSkuDetails("pro");
                    stringBuilder.append("," + vipDetails.getDescription());
                    updateUserStatusFromPurchase(pro);
                    updateUIifPro();
                }

                if (vip != null || pro != null) {
                    String statusString = stringBuilder.toString().replace(",", " и ");
                    mLabelMorePhoto.setText(statusString);
                }
            }
        });
    }


    private void updateUIifPro() {
        mBuyPro.setVisibility(View.GONE);
    }

    private void updateUIifVIP() {
        mBuyVip.setVisibility(View.GONE);
        mHideAge.setEnabled(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (Constants.IAB_ENABLED)
            iabHelper.dispose();
    }

    @Override
    public void onIabPurchaseFinished(IabResult result, Purchase info) {

        if (result.isSuccess()) {
            askForPurchase();
        } else {
            Toast.makeText(this, "Покупка уже соверщена или не доступна в данный момент", Toast.LENGTH_LONG).show();
        }
    }

    private void updateUserStatusFromPurchase(Purchase info) {


        if (info == null) return;

        String sku = info.getSku();

        Map<String, String> map = new HashMap<String, String>();

        if (!sku.equalsIgnoreCase("vip") && !sku.equalsIgnoreCase("pro")) return;

        User user = userProvider.load();

        if (sku.equalsIgnoreCase("pro")) {
            map.put("pro_status", "1");
            user.pro_status = true;
        }

        if (sku.equalsIgnoreCase("vip")) {
            map.put("vip_status", "1");
            user.vip_status = true;
        }

        userProvider.save(user);

        restManager.vinderService.updateSettings(userProvider.load().getID(),
                user.getSettings(), new Callback<Settings>() {
                    @Override
                    public void success(Settings settings, Response response) {
                        Toast.makeText(activity, "Статус обновлен", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Toast.makeText(activity, "Ошибка обновления статуса", Toast.LENGTH_LONG).show();
                    }
                });

    }


}
