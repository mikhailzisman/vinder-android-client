package main.java.com.app.vinderapp.IOC;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 16.06.2014
 * Time: 10:03
 */
public abstract class BootstrapFragmentDialog extends DialogFragment  {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Injector.inject(this);
        return super.onCreateDialog(savedInstanceState);
    }

}
