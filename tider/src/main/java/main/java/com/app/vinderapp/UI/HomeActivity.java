package main.java.com.app.vinderapp.UI;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.widget.SpinnerAdapter;
import main.java.com.app.vinderapp.Constants;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Utils.AccessPreferences;
import com.vk.sdk.VKSdk;
import com.vk.sdk.util.VKUtil;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 07.05.14
 * Time: 9:12
 */
public class HomeActivity extends ActionBarActivity {


    private HomeActivity homeActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.home_layout);
        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        AccessPreferences.commit(this, Constants.SCREEN_WIDTH, width);
        AccessPreferences.commit(this, Constants.SCREEN_HEIGHT, height);


        homeActivity = this;

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new VoteFragment())
                .commit();

    }

    @Override
    protected void onResume() {
        super.onResume();

        updateActionBar();
    }

    private void updateActionBar() {
        ActionBar.OnNavigationListener mOnNavigationListener = new ActionBar.OnNavigationListener() {
            @Override
            public boolean onNavigationItemSelected(int position, long l) {
                //from R.array.action_list

                //because first is empty
                position--;

                switch (position) {
                    case 1:
                        startActivity(new Intent(homeActivity, MessagesListActivity.class));
                        break;

                    case 2:
                        startActivity(new Intent(homeActivity, ProfileActivity.class));
                        break;

                    case 3:
                        startActivity(new Intent(homeActivity, SettingsActivity.class));
                        break;

                    case 4:
                        AlertDialog.Builder adb = new AlertDialog.Builder(homeActivity);
                        adb.setTitle("Выход из профиля").setMessage("Вы правда хотите выйти?")
                                .setPositiveButton("Да",new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        VKSdk.logout();
                                        finish();
                                    }
                                }).setNegativeButton("Нет",new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        }).show();

                        break;
                }


                return true;
            }
        };

        SpinnerAdapter spinnerAdapter = new DropDownMenuAdapter(this);

        getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

        getSupportActionBar().setListNavigationCallbacks(spinnerAdapter, mOnNavigationListener);
    }


}
