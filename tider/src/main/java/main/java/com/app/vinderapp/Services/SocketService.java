package main.java.com.app.vinderapp.Services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;
import main.java.com.app.vinderapp.Config;
import main.java.com.app.vinderapp.IOC.Injector;
import main.java.com.app.vinderapp.Models.Message;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.vk.sdk.VKSdk;
import io.socket.SocketIO;

import javax.inject.Inject;
import java.net.URI;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 05.06.2014
 * Time: 13:20
 */
public class SocketService extends Service {
    //http://milanito.github.io/web%20mobile/2013/12/29/sockets-with-android-and-sails/

    private static SocketIO socket;

    private SocketService context;
    private String TAG = "Socket service";
    private WebSocketClient client;
    private Handler mHandler;


    @Inject
    Gson gson;


    @Inject
    Bus BUS;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        Injector.inject(this);

        context = this;

        register();

        client = getClient();

        client.connect();

        return START_STICKY;
    }

    private WebSocketClient getClient() {
        return new WebSocketClient(URI.create(Config.socketSite), new WebSocketClient.Listener() {
            @Override
            public void onConnect() {
                Log.e(TAG, "Connected");
            }

            @Override
            public void onMessage(String message) {

                Log.e(TAG, "message");
                Message message1 = gson.fromJson(message, Message.class);

                sendMessage(message1);

            }

            @Override
            public void onMessage(byte[] data) {
                Log.e(TAG, "data");
            }

            @Override
            public void onDisconnect(int code, String reason) {
                Log.w(TAG, "disconnected");
            }

            @Override
            public void onError(Exception error) {
                Log.e(TAG, "error");
            }
        }, null);
    }

    private void sendMessage(Message message1) {

        Intent intent = new Intent("message");
        // add data
        intent.putExtra("message", message1);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);

        //BUS.post(message1);
    }



    public void register() {
        BUS.register(this);
    }

    public void unregister() {
        BUS.unregister(this);
    }

    @Override
    public void onDestroy() {
        unregister();
        super.onDestroy();
    }

}
