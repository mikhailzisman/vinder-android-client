package main.java.com.app.vinderapp.Services;

import android.location.Location;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 12.08.2014
 * Time: 9:20
 */
public class LocationUpdateEvent {
    public Location location;

    public LocationUpdateEvent(Location location) {
        this.location = location;
    }
}
