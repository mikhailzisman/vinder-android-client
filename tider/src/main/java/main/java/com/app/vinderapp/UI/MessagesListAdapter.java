package main.java.com.app.vinderapp.UI;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;
import main.java.com.app.vinderapp.IOC.Injector;
import main.java.com.app.vinderapp.Models.Friendship;
import main.java.com.app.vinderapp.Models.Like;
import main.java.com.app.vinderapp.Models.Message;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.DateUtils;
import main.java.com.app.vinderapp.Widget.RobotoRegularTextView;
import main.java.com.app.vinderapp.Widget.RobotoThinTextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.05.2014
 * Time: 9:30
 */
public class MessagesListAdapter extends ArrayAdapter<Friendship> {
    private final LayoutInflater inflater;
    private Context context;
    private List<Friendship> friendList;

    @Inject
    RestManager restManager;


    public MessagesListAdapter(Context context, int resource, ArrayList<Friendship> objects) {
        super(context, resource, objects);
        this.context = context;
        this.friendList = objects;
        inflater = LayoutInflater.from(context);

        Injector.inject(this);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        convertView = inflater.inflate(R.layout.messages_list_item, null);

        final Friendship frienddhip = getItem(position);

        final ViewHolder viewHolder = new ViewHolder(convertView);

        restManager.vinderService.findMessageFromFrendship(frienddhip, new Callback<ArrayList<Message>>() {
            @Override
            public void success(ArrayList<Message> messages, Response response) {
                int messagesCount = messages.size();

                Message lastMessage = null;
                if (messages.size() > 0)
                    lastMessage = messages.get(messagesCount - 1);

                if (lastMessage != null)
                    viewHolder.message.setText(lastMessage.getMessage());
                viewHolder.size.setText(String.valueOf(messagesCount));
                if (lastMessage != null)
                    viewHolder.time.setText(DateUtils.fromDatabaseDate(lastMessage.createdAt));

                viewHolder.name.setText(frienddhip.getHim().getFirst_name());
                ImageLoader.getInstance().displayImage(frienddhip.getLikeToUser1().getPhoto().getMaxResolutionUrl(), viewHolder.imageView);
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(context, "Не могу загрузить сообщения", Toast.LENGTH_LONG).show();
            }
        });
        return convertView;

    }


    public class ViewHolder {
        public final RobotoRegularTextView name;
        public final RobotoRegularTextView message;
        public final RobotoThinTextView time;
        public final View root;
        private final RobotoThinTextView size;
        public final ImageView imageView;

        public ViewHolder(View root) {
            name = (RobotoRegularTextView) root.findViewById(R.id.name);
            message = (RobotoRegularTextView) root.findViewById(R.id.message);
            time = (RobotoThinTextView) root.findViewById(R.id.time);
            size = (RobotoThinTextView) root.findViewById(R.id.size);
            imageView = (ImageView) root.findViewById(R.id.imageView);
            this.root = root;
        }
    }


}
