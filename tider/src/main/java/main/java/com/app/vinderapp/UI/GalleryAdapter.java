package main.java.com.app.vinderapp.UI;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 23.05.2014
 * Time: 11:33
 */

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import main.java.com.app.vinderapp.Models.Photo;
import main.java.com.app.vinderapp.R;

import java.util.List;

public class GalleryAdapter extends ArrayAdapter<Photo> {

    private Context context;
    private List<Photo> photos;

    public GalleryAdapter(Context context, int resource, List<Photo> photos) {
        super(context, resource, photos);
        this.context = context;
        this.photos = photos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView view = new ImageView(context);
        //view.setImageBitmap(photos.get(position).getBitmap());
        view.setPadding(20, 20, 20, 20);
        view.setLayoutParams(new Gallery.LayoutParams(250, 250));
        view.setScaleType(ImageView.ScaleType.FIT_XY);
        view.setBackgroundResource(R.drawable.empty_photo_resource);

        return view;
    }


}
