package main.java.com.app.vinderapp.Widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 21.05.2014
 * Time: 11:51
 */
public class RobotoThinTextView extends TextView {
    public RobotoThinTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        //Typeface.createFromAsset doesn't work in the layout editor. Skipping...
        if (isInEditMode()) {
            return;
        }


        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Thin.ttf");
        setTypeface(typeface);

    }
}
