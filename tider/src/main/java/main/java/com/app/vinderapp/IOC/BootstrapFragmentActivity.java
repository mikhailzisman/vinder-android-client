package main.java.com.app.vinderapp.IOC;

import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.FragmentActivity;
import butterknife.ButterKnife;

/**
 * Base class for all Bootstrap Activities that need fragments.
 */
public abstract class BootstrapFragmentActivity extends FragmentActivity {

    private PowerManager.WakeLock mWakeLock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        this.mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK, "My Tag");


        Injector.inject(this);
    }

    @Override
    public void setContentView(int layoutResId) {
        super.setContentView(layoutResId);

        ButterKnife.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mWakeLock.acquire();

    }

    @Override
    protected void onPause() {

        this.mWakeLock.release();
        super.onPause();
    }


}
