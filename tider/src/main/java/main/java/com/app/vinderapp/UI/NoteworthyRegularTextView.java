package main.java.com.app.vinderapp.UI;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 20.06.2014
 * Time: 9:58
 */
public class NoteworthyRegularTextView extends TextView {
    public NoteworthyRegularTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        //Typeface.createFromAsset doesn't work in the layout editor. Skipping...
        if (isInEditMode()) {
            return;
        }


        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Noteworthy-Bold.ttf");
        setTypeface(typeface);

    }
}
