package main.java.com.app.vinderapp.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 17.06.2014
 * Time: 12:27
 */
public class JSONUtils {

    public static boolean isJSONValid(String mayBeJson) {
        try {
            new JSONObject(mayBeJson);
        } catch (JSONException ex) {
            try {
                new JSONArray(mayBeJson);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

}
