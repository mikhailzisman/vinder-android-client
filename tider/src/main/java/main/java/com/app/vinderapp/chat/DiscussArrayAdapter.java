package main.java.com.app.vinderapp.chat;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 06.05.14
 * Time: 10:57
 */

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import main.java.com.app.vinderapp.IOC.Injector;
import main.java.com.app.vinderapp.Models.Friendship;
import main.java.com.app.vinderapp.Models.Message;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.DateUtils;
import main.java.com.app.vinderapp.Utils.UserProvider;
import com.nostra13.universalimageloader.core.ImageLoader;

import javax.inject.Inject;

public class DiscussArrayAdapter extends ArrayAdapter<Message> {

    @Inject
    RestManager restManager;

    private TextView textView;
    //private List<Message> countries = new ArrayList<Message>();
    private LinearLayout wrapper;
    private Context context;
    private Friendship friendship;

    @Inject
    UserProvider userProvider;

    @Override
    public void add(Message object) {
        //countries.add(object);
        super.add(object);
    }

    public DiscussArrayAdapter(Context context, int textViewResourceId, Friendship friendship) {
        super(context, textViewResourceId);
        this.context = context;
        this.friendship = friendship;

        Injector.inject(this);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        row = inflater.inflate(R.layout.listitem_discuss, parent, false);

        wrapper = (LinearLayout) row.findViewById(R.id.wrapper);

        Message message = getItem(position);

        textView = (TextView) row.findViewById(R.id.comment);

        textView.setText(message.getMessage());

        TextView date = (TextView) row.findViewById(R.id.wasTime);
        if (message.createdAt != null)
            date.setText(DateUtils.fromDatabaseDate(message.createdAt));

        ViewGroup viewGroup = (ViewGroup) wrapper.findViewById(isOwnerOfMessage(message) ? R.id.right : R.id.left);

        LinearLayout inflate = (LinearLayout) View.inflate(context, R.layout.chat_photo_layout, null);
        viewGroup.addView(inflate);

        final ImageView chat_photo = (ImageView) inflate.findViewById(R.id.photo);

        ImageLoader.getInstance().displayImage(isOwnerOfMessage(message) ?
                        friendship.getLikeToUser1().getPhoto().getMaxResolutionUrl() :
                        friendship.getLikeToUser2().getPhoto().getMaxResolutionUrl(),
                chat_photo);


        textView.setBackgroundResource(R.drawable.chat_message_background);
        wrapper.setGravity(isOwnerOfMessage(message) ? Gravity.RIGHT : Gravity.LEFT);

        inflate.requestLayout();
        wrapper.requestLayout();


        return row;
    }

    private boolean isOwnerOfMessage(Message message) {
        return message.getWho().getID().equals(userProvider.load().getID());
    }
}
