package main.java.com.app.vinderapp.Services;

import android.location.Location;
import main.java.com.app.vinderapp.Config;
import main.java.com.app.vinderapp.IOC.Injector;
import main.java.com.app.vinderapp.Models.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 30.05.2014
 * Time: 10:22
 */
public class RestManager {


    private RestAdapter restJSONAdapter;

    public VinderService vinderService;

    @Inject
    Gson gson;

    public RestManager() {

        Injector.inject(this);

        restJSONAdapter = new RestAdapter.Builder()
                .setEndpoint(Config.site)
                .setConverter(new GsonConverter(gson))
                .build();


        vinderService = restJSONAdapter.create(VinderService.class);

    }

    ///////////////////////PHOTO////////////////////////////////////

    public interface VinderService {    ////////////

        /////////////////////////PHOTO////////////////////////////////
        @POST("/photos/create/{user_id}")
        void createPhoto(
                @Path("user_id") Long user_id,@Body Photo photo,
                Callback<Photo> responseCallback);

        @GET("/photos/delete/{id}")
        void destroy(@Path("id") Long id, Callback<Photo> deleteCallback);

        @GET("/photos/getByUserId/{user_id}")
        void getPhotosByUserId(
                @Path("user_id") Long id,
                Callback<ArrayList<Photo>> responseCallback);


        @GET("/photos/get/{id}")
        void getPhoto(@Path("id") Long photo_id, Callback<Photo> findPhotoCallback);


        @GET("/photos/getNext/{user_id}")
        void getNextPhoto(@Path("user_id") Long user_id, Callback<Photo> photoCallback);

        @GET("/photos/getDisliked/{user_id}")
        void getDislikedPhotos(@Path("user_id") Long user_id, Callback<ArrayList<Like>> photos);



        //////////////////USERS///////////////////////////////////
        @POST("/users/create")
        void createUser(
                @Body User user,
                Callback<User> responseCallback);

        @GET("/users/get/{id}")
        void getUser(
                @Path("id") Long id,
                Callback<User> responseCallback);

        @GET("/users/getByUid/{user_uid}")
        void getUserByUid(
                @Path("user_uid") Long user_uid,
                Callback<User> responseCallback);


        @POST("/users/update/{id}")
        void updateUser(@Path("id") Long id,
                        @Body User user,
                        Callback<User> updateUserCallback);

        @POST("/users/updateLocations/{user_id}")
        void updateLocation(@Path("user_id") Long user_id, @Body Location location);

        @POST("/users/updateSettings/{user_id}")
        void updateSettings(@Path("user_id") Long id, @Body Settings settings, Callback<Settings> callback);


        //////////////////LIKES///////////////////////////////////
        @POST("/likes/create")
        void createLike(
                @Body Like like,
                Callback<Like> responseCallback);

        @GET("/likes/get/{who_id}/{whom_id}")
        void getLikeBetweenUsers(
                @Path("who_id") String who_id, @Path("whom_id") String whom_id,
                Callback<ArrayList<Like>> responseCallback);

        @GET("/likes/get/{id}")
        void getLike(
                @Path("id") String id,
                Callback<Like> responseCallback);


        @POST("/likes/update")
        void updateLike(@Body Like like, Callback<Like> updateLikeCallback);


        /////////////////MESSAGES////////////////////////////////////////////

        @GET("/messages/get/{who_id}/{whom_id}")
        void getMessagesBetweenUsers(
                @Path("who_id") String who_id, @Path("whom_id") String whom_id,
                Callback<ArrayList<Message>> responseCallback);

        @GET("/messages/get/{id}")
        void getMessage(
                @Path("id") Long id ,
                Callback<ArrayList<Message>> responseCallback);

        @POST("/messages/create")
        void createMessage(@Body Message message, Callback<Message> messageCreateCallback);

        ///////////////INVITATIONS///////////////////////////////////////////

        @POST("/friendships/update")
        void updateFriendship(
                @Body Friendship friendship,
                Callback<Friendship> responseCallback);

        @GET("/invitations/get/{who_id}/{whom_id}")
        void getFriendship(
                @Path("who_id") String who_id, @Path("whom_id") String whom_id,
                Callback<ArrayList<Friendship>> responseCallback);

        @POST("/messages/getFromFriendship")
        void findMessageFromFrendship(@Body Friendship friendship, Callback<ArrayList<Message>> messages);

        @GET("/friendships/get/{user_id}")
        void getFriendshipsToMe(@Path("user_id") Long user_id,
                                Callback<ArrayList<Friendship>> responseCallback);

    }


}
