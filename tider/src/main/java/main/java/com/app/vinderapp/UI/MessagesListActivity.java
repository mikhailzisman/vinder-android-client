package main.java.com.app.vinderapp.UI;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import butterknife.InjectView;
import butterknife.OnClick;
import main.java.com.app.vinderapp.IOC.BootstrapFragmentActivity;
import main.java.com.app.vinderapp.Models.Friendship;
import main.java.com.app.vinderapp.R;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.UserProvider;
import main.java.com.app.vinderapp.chat.ChatActivity;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Mikhail
 * Date: 27.05.2014
 * Time: 9:26
 */
public class MessagesListActivity extends BootstrapFragmentActivity {

    @Inject
    RestManager restManager;

    @Inject
    UserProvider userProvider;

    @InjectView(R.id.listView)
    ListView mListView;

    @InjectView(android.R.id.empty)
    ViewStub mEmpty;

    @InjectView(R.id.notMessagesFoundLayout)
    LinearLayout messagesNotFound;

    @OnClick(R.id.returnToVote)
    public void returnButton() {
        finish();
    }

    private MessagesListAdapter messagesListAdapter;
    private MessagesListActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;

        setContentView(R.layout.messages_list);

        ArrayList<Friendship> friendships = new ArrayList<Friendship>();

        messagesListAdapter = new MessagesListAdapter(this, 0, friendships);

        mListView.setAdapter(messagesListAdapter);

        restManager.vinderService.getFriendshipsToMe(userProvider.load().getID(), new Callback<ArrayList<Friendship>>() {
            @Override
            public void success(ArrayList<Friendship> friendships, Response response) {

                messagesNotFound.setVisibility(friendships.isEmpty() ? View.VISIBLE : View.INVISIBLE);
                mListView.setVisibility(friendships.isEmpty() ? View.INVISIBLE : View.VISIBLE);

                messagesListAdapter.clear();
                messagesListAdapter.addAll(friendships);
                messagesListAdapter.notifyDataSetChanged();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(activity, "Ошибка чтения сообщений", Toast.LENGTH_LONG).show();
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Friendship friendship = messagesListAdapter.getItem(position);
                if (friendship == null) return;

                gotoChatActivityWithFriendship(friendship);
            }
        });
    }

    private void gotoChatActivityWithFriendship(Friendship friendship) {

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.FRIENDSHIP, friendship);
        startActivity(intent);
    }
}
