package main.java.com.app.vinderapp.UI;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import main.java.com.app.vinderapp.Config;
import main.java.com.app.vinderapp.IOC.BootstrapFragmentActivity;
import main.java.com.app.vinderapp.Models.Settings;
import main.java.com.app.vinderapp.Models.User;
import main.java.com.app.vinderapp.Services.RestManager;
import main.java.com.app.vinderapp.Utils.UserInfoUtils;
import main.java.com.app.vinderapp.Utils.UserProvider;
import com.google.gson.Gson;
import com.squareup.otto.Bus;
import com.vk.sdk.*;
import com.vk.sdk.api.*;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.dialogs.VKCaptchaDialog;
import com.vk.sdk.util.VKUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import javax.inject.Inject;


/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends BootstrapFragmentActivity {

    @Inject
    RestManager restManager;

    @Inject
    UserProvider userProvider;

    @Inject
    Bus BUS;
    /**
     * Scope is set of required permissions for your application
     *
     * @see <a href="https://vk.com/dev/permissions">vk.com api permissions documentation</a>
     */
    private static final String[] sMyScope = new String[]{
            VKScope.FRIENDS,
            //VKScope.WALL,
            //VKScope.PHOTOS,
            VKScope.NOHTTPS
    };
    private LoginActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        setContentView(main.java.com.app.vinderapp.R.layout.activity_start);
        VKUIHelper.onCreate(this);
        VKSdk.initialize(sdkListener, Config.vk_id_app);
        if (VKSdk.wakeUpSession()) {

            restManager.vinderService.getUserByUid(Long.parseLong(VKSdk.getAccessToken().userId), new Callback<User>() {
                @Override
                public void success(User user, Response response) {
                    userProvider.save(user);
                    startHomeActivity();
                }

                @Override
                public void failure(RetrofitError retrofitError) {
                    startGetUserInfo();
                }
            });
            return;
        }

        String[] fingerprint = VKUtil.getCertificateFingerprint(this, this.getPackageName());
        Log.d("Fingerprint", fingerprint[0]);
    }

    private void showLogout() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(main.java.com.app.vinderapp.R.id.container, new LogoutFragment())
                .commit();
    }

    private void showLogin() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(main.java.com.app.vinderapp.R.id.container, new LoginFragment())
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        VKUIHelper.onResume(this);
        if (VKSdk.isLoggedIn()) {
            showLogout();
        } else {
            showLogin();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        VKUIHelper.onDestroy(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        VKUIHelper.onActivityResult(requestCode, resultCode, data);
    }

    private final VKSdkListener sdkListener = new VKSdkListener() {
        @Override
        public void onCaptchaError(VKError captchaError) {
            new VKCaptchaDialog(captchaError).show();
        }

        @Override
        public void onTokenExpired(VKAccessToken expiredToken) {
            VKSdk.authorize(sMyScope);
        }

        @Override
        public void onAccessDenied(VKError authorizationError) {
            new AlertDialog.Builder(LoginActivity.this)
                    .setMessage(authorizationError.errorMessage)
                    .show();
        }

        @Override
        public void onReceiveNewToken(VKAccessToken newToken) {
            startGetUserInfo();
        }

        @Override
        public void onAcceptUserToken(VKAccessToken token) {
            startGetUserInfo();
        }
    };

    @Inject
    Gson gson;

    private void startGetUserInfo() {
        //VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, VKSdk.getAccessToken().userId));

        //FOR CURRENT USER (OWNER APP)
        VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS,
                "id,first_name,last_name,sex,bdate,city,country,photo_50,photo_100," +
                        "photo_200_orig,photo_200,photo_400_orig,photo_max,photo_max_orig,online," +
                        "online_mobile,lists,domain,has_mobile,contacts,connections,site,education," +
                        "universities,schools,can_post,can_see_all_posts,can_see_audio,can_write_private_message," +
                        "status,last_seen,common_count,relation,relatives,counters"
        ));
        request.secure = true;
        request.useSystemLanguage = false;
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);

                VKList<VKApiUserFull> vkApiUserFullList = (VKList<VKApiUserFull>) response.parsedModel;
                if (vkApiUserFullList.size() == 1) {

                    userProvider.save(vkApiUserFullList.get(0));

                    User user = new User();
                    user.setUid((long) userProvider.loadVKInfo().id);
                    user.setLast_name(userProvider.loadVKInfo().last_name);
                    user.setFirst_name(userProvider.loadVKInfo().first_name);
                    user.setSex(userProvider.loadVKInfo().sex);
                    user.setAge(UserInfoUtils.getAgesAsInt(userProvider.loadVKInfo().bdate));

                    user.setSettings(new Settings());

                    userProvider.save(user);

                    restManager.vinderService.getUserByUid(userProvider.load().getUid(), new Callback<User>() {
                        @Override
                        public void success(User user, Response response) {
                            userProvider.save(user);
                            startHomeActivity();
                        }

                        @Override
                        public void failure(RetrofitError retrofitError) {
                            if (userProvider.load() != null)
                                createUser();
                        }
                    });

                } else
                    Toast.makeText(activity, "Странно, больше одной записи по одному id", Toast.LENGTH_LONG).show();


            }

            @Override
            public void onError(VKError error) {
                super.onError(error);
                Toast.makeText(activity, "Ошибка при загрузке данных из Вконтакте: " + error.errorMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void createUser() {


        restManager.vinderService.createUser(userProvider.load(), new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                userProvider.save(user);
                startHomeActivity();
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(activity, "Ошибка создания пользователя", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void startHomeActivity() {
        startActivity(new Intent(this, HomeActivity.class));
    }


    public static class LoginFragment extends android.support.v4.app.Fragment {
        public LoginFragment() {
            super();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(main.java.com.app.vinderapp.R.layout.splash_screen, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            getView().findViewById(main.java.com.app.vinderapp.R.id.button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    VKSdk.authorize(sMyScope, true, false);
                }
            });
        }
    }

    public static class LogoutFragment extends android.support.v4.app.Fragment {
        public LogoutFragment() {
            super();
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            return inflater.inflate(main.java.com.app.vinderapp.R.layout.fragment_logout, container, false);
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            getView().findViewById(main.java.com.app.vinderapp.R.id.continue_button).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((LoginActivity) getActivity()).startHomeActivity();
                }
            });

            getView().findViewById(main.java.com.app.vinderapp.R.id.logout).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    VKSdk.logout();
                    if (!VKSdk.isLoggedIn()) {
                        ((LoginActivity) getActivity()).showLogin();
                    }
                }
            });
        }
    }
}
