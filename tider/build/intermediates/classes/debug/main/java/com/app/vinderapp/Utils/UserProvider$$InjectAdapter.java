// Code generated by dagger-compiler.  Do not edit.
package main.java.com.app.vinderapp.Utils;

import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;
import javax.inject.Provider;

/**
 * A {@code Binding<UserProvider>} implementation which satisfies
 * Dagger's infrastructure requirements including:
 *
 * Owning the dependency links between {@code UserProvider} and its
 * dependencies.
 *
 * Being a {@code Provider<UserProvider>} and handling creation and
 * preparation of object instances.
 *
 * Being a {@code MembersInjector<UserProvider>} and handling injection
 * of annotated fields.
 */
public final class UserProvider$$InjectAdapter extends Binding<UserProvider>
    implements Provider<UserProvider>, MembersInjector<UserProvider> {
  private Binding<com.google.gson.Gson> gson;
  private Binding<android.content.Context> context;

  public UserProvider$$InjectAdapter() {
    super("main.java.com.app.vinderapp.Utils.UserProvider", "members/main.java.com.app.vinderapp.Utils.UserProvider", NOT_SINGLETON, UserProvider.class);
  }

  /**
   * Used internally to link bindings/providers together at run time
   * according to their dependency graph.
   */
  @Override
  @SuppressWarnings("unchecked")
  public void attach(Linker linker) {
    gson = (Binding<com.google.gson.Gson>) linker.requestBinding("com.google.gson.Gson", UserProvider.class, getClass().getClassLoader());
    context = (Binding<android.content.Context>) linker.requestBinding("android.content.Context", UserProvider.class, getClass().getClassLoader());
  }

  /**
   * Used internally obtain dependency information, such as for cyclical
   * graph detection.
   */
  @Override
  public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> injectMembersBindings) {
    injectMembersBindings.add(gson);
    injectMembersBindings.add(context);
  }

  /**
   * Returns the fully provisioned instance satisfying the contract for
   * {@code Provider<UserProvider>}.
   */
  @Override
  public UserProvider get() {
    UserProvider result = new UserProvider();
    injectMembers(result);
    return result;
  }

  /**
   * Injects any {@code @Inject} annotated fields in the given instance,
   * satisfying the contract for {@code Provider<UserProvider>}.
   */
  @Override
  public void injectMembers(UserProvider object) {
    object.gson = gson.get();
    object.context = context.get();
  }

}
