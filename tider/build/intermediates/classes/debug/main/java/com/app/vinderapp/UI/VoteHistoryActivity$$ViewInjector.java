// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class VoteHistoryActivity$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.VoteHistoryActivity target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296439, "field 'mProPanel'");
    target.mProPanel = (android.widget.LinearLayout) view;
    view = finder.findRequiredView(source, 2131296440, "field 'mNotLikesFoundLayout'");
    target.mNotLikesFoundLayout = (android.widget.LinearLayout) view;
    view = finder.findRequiredView(source, 2131296352, "method 'gotoVoteScreen'");
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.gotoVoteScreen();
        }
      });
    view = finder.findRequiredView(source, 2131296399, "method 'buyVIP'");
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.buyVIP();
        }
      });
  }

  public static void reset(main.java.com.app.vinderapp.UI.VoteHistoryActivity target) {
    target.mProPanel = null;
    target.mNotLikesFoundLayout = null;
  }
}
