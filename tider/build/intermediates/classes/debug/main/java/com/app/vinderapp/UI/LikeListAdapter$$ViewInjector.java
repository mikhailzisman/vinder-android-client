// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class LikeListAdapter$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.LikeListAdapter target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296365, "field 'imageView'");
    target.imageView = (android.widget.ImageView) view;
  }

  public static void reset(main.java.com.app.vinderapp.UI.LikeListAdapter target) {
    target.imageView = null;
  }
}
