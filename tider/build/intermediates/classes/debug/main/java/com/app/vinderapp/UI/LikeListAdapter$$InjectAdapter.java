// Code generated by dagger-compiler.  Do not edit.
package main.java.com.app.vinderapp.UI;

import dagger.MembersInjector;
import dagger.internal.Binding;
import dagger.internal.Linker;
import java.util.Set;

/**
 * A {@code Binding<LikeListAdapter>} implementation which satisfies
 * Dagger's infrastructure requirements including:
 *
 * Owning the dependency links between {@code LikeListAdapter} and its
 * dependencies.
 *
 * Being a {@code Provider<LikeListAdapter>} and handling creation and
 * preparation of object instances.
 *
 * Being a {@code MembersInjector<LikeListAdapter>} and handling injection
 * of annotated fields.
 */
public final class LikeListAdapter$$InjectAdapter extends Binding<LikeListAdapter>
    implements MembersInjector<LikeListAdapter> {
  private Binding<main.java.com.app.vinderapp.Services.RestManager> restManager;

  public LikeListAdapter$$InjectAdapter() {
    super(null, "members/main.java.com.app.vinderapp.UI.LikeListAdapter", NOT_SINGLETON, LikeListAdapter.class);
  }

  /**
   * Used internally to link bindings/providers together at run time
   * according to their dependency graph.
   */
  @Override
  @SuppressWarnings("unchecked")
  public void attach(Linker linker) {
    restManager = (Binding<main.java.com.app.vinderapp.Services.RestManager>) linker.requestBinding("main.java.com.app.vinderapp.Services.RestManager", LikeListAdapter.class, getClass().getClassLoader());
  }

  /**
   * Used internally obtain dependency information, such as for cyclical
   * graph detection.
   */
  @Override
  public void getDependencies(Set<Binding<?>> getBindings, Set<Binding<?>> injectMembersBindings) {
    injectMembersBindings.add(restManager);
  }

  /**
   * Injects any {@code @Inject} annotated fields in the given instance,
   * satisfying the contract for {@code Provider<LikeListAdapter>}.
   */
  @Override
  public void injectMembers(LikeListAdapter object) {
    object.restManager = restManager.get();
  }

}
