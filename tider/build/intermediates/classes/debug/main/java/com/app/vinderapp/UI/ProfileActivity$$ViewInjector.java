// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class ProfileActivity$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.ProfileActivity target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296378, "field 'tvName'");
    target.tvName = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131296421, "field 'tvFriends'");
    target.tvFriends = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131296401, "field 'mDifference'");
    target.mDifference = (main.java.com.app.vinderapp.Widget.RobotoRegularTextView) view;
    view = finder.findRequiredView(source, 2131296422, "field 'mPro'");
    target.mPro = (main.java.com.app.vinderapp.Widget.RobotoThinTextView) view;
    view = finder.findRequiredView(source, 2131296423, "field 'mDistance'");
    target.mDistance = (main.java.com.app.vinderapp.Widget.RobotoThinTextView) view;
    view = finder.findRequiredView(source, 2131296403, "field 'mHideAge'");
    target.mHideAge = (android.widget.CheckBox) view;
    view = finder.findRequiredView(source, 2131296402, "field 'mHideAgeLayout'");
    target.mHideAgeLayout = (android.widget.LinearLayout) view;
    view = finder.findRequiredView(source, 2131296399, "field 'mBuyVip' and method 'buyVIP'");
    target.mBuyVip = (android.widget.Button) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.buyVIP();
        }
      });
    view = finder.findRequiredView(source, 2131296400, "field 'mBuyPro' and method 'buyPro'");
    target.mBuyPro = (android.widget.Button) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.buyPro();
        }
      });
    view = finder.findRequiredView(source, 2131296347, "field 'mUpProfile' and method 'upProfile'");
    target.mUpProfile = (android.widget.LinearLayout) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.upProfile();
        }
      });
    view = finder.findRequiredView(source, 2131296398, "field 'mLabelMorePhoto'");
    target.mLabelMorePhoto = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131296350, "field 'tags' and method 'editTags'");
    target.tags = (android.widget.TextView) view;
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.editTags();
        }
      });
  }

  public static void reset(main.java.com.app.vinderapp.UI.ProfileActivity target) {
    target.tvName = null;
    target.tvFriends = null;
    target.mDifference = null;
    target.mPro = null;
    target.mDistance = null;
    target.mHideAge = null;
    target.mHideAgeLayout = null;
    target.mBuyVip = null;
    target.mBuyPro = null;
    target.mUpProfile = null;
    target.mLabelMorePhoto = null;
    target.tags = null;
  }
}
