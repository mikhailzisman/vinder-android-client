// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class ProfileBuddy$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.ProfileBuddy target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296378, "field 'name'");
    target.name = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131296421, "field 'friends'");
    target.friends = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131296423, "field 'dsitance'");
    target.dsitance = (android.widget.TextView) view;
  }

  public static void reset(main.java.com.app.vinderapp.UI.ProfileBuddy target) {
    target.name = null;
    target.friends = null;
    target.dsitance = null;
  }
}
