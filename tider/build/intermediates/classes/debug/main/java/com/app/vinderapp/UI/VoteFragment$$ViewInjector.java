// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class VoteFragment$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.VoteFragment target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296374, "field 'mWasTime'");
    target.mWasTime = (main.java.com.app.vinderapp.Widget.RobotoThinTextView) view;
    view = finder.findRequiredView(source, 2131296422, "field 'mPro'");
    target.mPro = (main.java.com.app.vinderapp.Widget.RobotoThinTextView) view;
    view = finder.findRequiredView(source, 2131296378, "field 'name'");
    target.name = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131296421, "field 'friends'");
    target.friends = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131296423, "field 'dsitance'");
    target.dsitance = (android.widget.TextView) view;
    view = finder.findRequiredView(source, 2131296442, "field 'notUserFoundLayout'");
    target.notUserFoundLayout = (android.widget.LinearLayout) view;
    view = finder.findRequiredView(source, 2131296445, "field 'userInfoLayout'");
    target.userInfoLayout = (android.widget.LinearLayout) view;
    view = finder.findRequiredView(source, 2131296443, "method 'onChangeSettings'");
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.onChangeSettings();
        }
      });
  }

  public static void reset(main.java.com.app.vinderapp.UI.VoteFragment target) {
    target.mWasTime = null;
    target.mPro = null;
    target.name = null;
    target.friends = null;
    target.dsitance = null;
    target.notUserFoundLayout = null;
    target.userInfoLayout = null;
  }
}
