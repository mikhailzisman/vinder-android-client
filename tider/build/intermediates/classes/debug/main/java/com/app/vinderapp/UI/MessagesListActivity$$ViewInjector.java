// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class MessagesListActivity$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.MessagesListActivity target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296375, "field 'mListView'");
    target.mListView = (android.widget.ListView) view;
    view = finder.findRequiredView(source, 16908292, "field 'mEmpty'");
    target.mEmpty = (android.view.ViewStub) view;
    view = finder.findRequiredView(source, 2131296376, "field 'messagesNotFound'");
    target.messagesNotFound = (android.widget.LinearLayout) view;
    view = finder.findRequiredView(source, 2131296377, "method 'returnButton'");
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.returnButton();
        }
      });
  }

  public static void reset(main.java.com.app.vinderapp.UI.MessagesListActivity target) {
    target.mListView = null;
    target.mEmpty = null;
    target.messagesNotFound = null;
  }
}
