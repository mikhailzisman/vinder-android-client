// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class FullPhotoFragmentDialog$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.FullPhotoFragmentDialog target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296365, "field 'mImageView'");
    target.mImageView = (android.widget.ImageView) view;
    view = finder.findRequiredView(source, 2131296342, "field 'mLayout'");
    target.mLayout = (android.widget.RelativeLayout) view;
  }

  public static void reset(main.java.com.app.vinderapp.UI.FullPhotoFragmentDialog target) {
    target.mImageView = null;
    target.mLayout = null;
  }
}
