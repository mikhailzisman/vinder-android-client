// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class InvitationFragmentActivity$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.InvitationFragmentActivity target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296344, "field 'mMyImageView'");
    target.mMyImageView = (android.widget.ImageView) view;
    view = finder.findRequiredView(source, 2131296345, "field 'mHimImageView'");
    target.mHimImageView = (android.widget.ImageView) view;
    view = finder.findRequiredView(source, 2131296346, "method 'yes'");
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.yes();
        }
      });
    view = finder.findRequiredView(source, 2131296348, "method 'no'");
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.no();
        }
      });
  }

  public static void reset(main.java.com.app.vinderapp.UI.InvitationFragmentActivity target) {
    target.mMyImageView = null;
    target.mHimImageView = null;
  }
}
