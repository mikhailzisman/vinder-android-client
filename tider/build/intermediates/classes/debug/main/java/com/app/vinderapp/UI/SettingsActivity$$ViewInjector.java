// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class SettingsActivity$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.SettingsActivity target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296410, "field 'mSave'");
    target.mSave = (android.widget.Button) view;
    view = finder.findRequiredView(source, 2131296414, "field 'mDistanceSeekBar'");
    target.mDistanceSeekBar = (android.widget.SeekBar) view;
    view = finder.findRequiredView(source, 2131296417, "field 'mRangeSeekBar'");
    target.mRangeSeekBar = (android.widget.FrameLayout) view;
    view = finder.findRequiredView(source, 2131296418, "field 'mFilterCheckBox' and method 'onChecked'");
    target.mFilterCheckBox = (android.widget.CheckBox) view;
    ((android.widget.CompoundButton) view).setOnCheckedChangeListener(
      new android.widget.CompoundButton.OnCheckedChangeListener() {
        @Override public void onCheckedChanged(
          android.widget.CompoundButton p0,
          boolean p1
        ) {
          target.onChecked(p1);
        }
      });
    view = finder.findRequiredView(source, 2131296411, "field 'mSex'");
    target.mSex = (android.widget.Switch) view;
    view = finder.findRequiredView(source, 2131296412, "field 'mDistanceMax'");
    target.mDistanceMax = (main.java.com.app.vinderapp.Widget.RobotoThinTextView) view;
    view = finder.findRequiredView(source, 2131296413, "field 'mDistanceMin'");
    target.mDistanceMin = (main.java.com.app.vinderapp.Widget.RobotoThinTextView) view;
    view = finder.findRequiredView(source, 2131296415, "field 'mAgeMax'");
    target.mAgeMax = (main.java.com.app.vinderapp.Widget.RobotoThinTextView) view;
    view = finder.findRequiredView(source, 2131296416, "field 'mAgeMin'");
    target.mAgeMin = (main.java.com.app.vinderapp.Widget.RobotoThinTextView) view;
    view = finder.findRequiredView(source, 2131296419, "field 'filterSwitcher'");
    target.filterSwitcher = (android.widget.Switch) view;
  }

  public static void reset(main.java.com.app.vinderapp.UI.SettingsActivity target) {
    target.mSave = null;
    target.mDistanceSeekBar = null;
    target.mRangeSeekBar = null;
    target.mFilterCheckBox = null;
    target.mSex = null;
    target.mDistanceMax = null;
    target.mDistanceMin = null;
    target.mAgeMax = null;
    target.mAgeMin = null;
    target.filterSwitcher = null;
  }
}
