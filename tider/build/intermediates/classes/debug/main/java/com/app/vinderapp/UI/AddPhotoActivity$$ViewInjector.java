// Generated code from Butter Knife. Do not modify!
package main.java.com.app.vinderapp.UI;

import android.view.View;
import butterknife.ButterKnife.Finder;

public class AddPhotoActivity$$ViewInjector {
  public static void inject(Finder finder, final main.java.com.app.vinderapp.UI.AddPhotoActivity target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296349, "method 'onSave'");
    view.setOnClickListener(
      new android.view.View.OnClickListener() {
        @Override public void onClick(
          android.view.View p0
        ) {
          target.onSave();
        }
      });
  }

  public static void reset(main.java.com.app.vinderapp.UI.AddPhotoActivity target) {
  }
}
